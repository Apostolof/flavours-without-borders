SELECT *
FROM (
    SELECT DISTINCT restaurant_id,
    restaurant_name,
    restaurant_category,
    restaurant_longitude,
    restaurant_latitude,
    restaurant_opening,
    restaurant_closing,
    user_id, p.radius,
        p.distance_unit
        * DEGREES(ACOS(COS(RADIANS(p.latpoint))
        * COS(RADIANS(z.restaurant_latitude))
        * COS(RADIANS(p.longpoint - z.restaurant_longitude))
        + SIN(RADIANS(p.latpoint))
        * SIN(RADIANS(z.restaurant_latitude)))) AS distance
    FROM users_view AS z
    JOIN (   /* these are the query parameters */
        SELECT 22.953012 AS latpoint, 40.635502 AS longpoint, 0.7 AS radius, 111.045 AS distance_unit
    ) AS p ON 1=1
    WHERE z.restaurant_latitude
        BETWEEN p.latpoint  - (p.radius / p.distance_unit)
            AND p.latpoint  + (p.radius / p.distance_unit)
    AND z.restaurant_longitude
        BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
            AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
    ) AS d
WHERE distance <= radius
ORDER BY distance
LIMIT 15

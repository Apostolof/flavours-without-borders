CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient` (
  `ingredient_name` varchar(500) NOT NULL,
  `ingredient_has_alcohol` bit(1) DEFAULT b'0',
  PRIMARY KEY (`ingredient_name`),
  UNIQUE KEY `ingredient_name_UNIQUE` (`ingredient_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient`
--

LOCK TABLES `ingredient` WRITE;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` VALUES ('Angostura bitters',_binary ''),('club soda',_binary '\0'),('coffee liqueur',_binary '\0'),('Cola',_binary '\0'),('cream',_binary '\0'),('Gin',_binary ''),('Lemon juice',_binary '\0'),('Lemon slice',_binary '\0'),('lime',_binary '\0'),('mint',_binary '\0'),('rum',_binary ''),('Silver tequila',_binary ''),('sugar',_binary '\0'),('Sweet and sour mix',_binary '\0'),('Triple sec',_binary ''),('vodka',_binary ''),('whiskey',_binary ''),('White rum',_binary ''),('αλάτι',_binary '\0'),('κιμάς',_binary '\0'),('κουκουνάρι',_binary '\0'),('κρεμμύδι',_binary '\0'),('λάδι',_binary '\0'),('μαϊντανός',_binary '\0'),('μοσχαρίσια πόδια',_binary '\0'),('μπούκοβο',_binary '\0'),('ντομάτα',_binary '\0'),('πατάτα',_binary '\0'),('πιπεριά',_binary '\0'),('ρύζι',_binary '\0'),('σκόρδο',_binary '\0'),('ψάρι',_binary '\0');
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:54:01

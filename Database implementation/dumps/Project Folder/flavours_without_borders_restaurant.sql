CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant` (
  `restaurant_id` int(11) NOT NULL,
  `restaurant_name` varchar(500) NOT NULL,
  `restaurant_category` enum('cafeteria','pub','bar','restaurant','fast_food','ethnic') NOT NULL,
  `restaurant_longitude` float NOT NULL,
  `restaurant_latitude` float NOT NULL,
  `restaurant_opening` time DEFAULT NULL,
  `restaurant_closing` time DEFAULT NULL,
  `restaurant_is_approved` bit(1) DEFAULT b'0' COMMENT 'Ίσως πρέπει να γίνει NN και να φύγει το default;',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`restaurant_id`),
  UNIQUE KEY `restaurant_id_UNIQUE` (`restaurant_id`),
  KEY `restaurant_is_owned_by_user_id_idx` (`user_id`),
  CONSTRAINT `restaurant_is_owned_by_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant`
--

LOCK TABLES `restaurant` WRITE;
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
INSERT INTO `restaurant` VALUES (7,'U Fleků','pub',40.6357,22.9367,'18:00:00','04:00:00',_binary '\0',NULL),(8,'Σ σύγχρονα εστιατόρια','restaurant',40.6173,22.9597,'12:00:00','22:00:00',_binary '',NULL),(9,'U Fleků Garden','pub',40.6354,22.937,'18:00:00','02:00:00',_binary '',NULL),(15,'McDonald\'s','fast_food',40.6463,22.9206,'12:00:00','23:59:00',_binary '',12),(129,'Grill 15','fast_food',40.6215,22.961,'12:00:00','01:30:00',_binary '\0',NULL),(236,'Butterflies and Hurricanes','cafeteria',40.6147,22.9609,'08:00:00','20:00:00',_binary '',356),(683,'The Hoppy Pub','pub',40.6279,22.9489,'17:30:00','01:30:00',_binary '',NULL),(768,'Belleville sin patron','bar',40.6337,22.9518,'12:00:00','02:00:00',_binary '',7),(1356,'Αιθερόπλοο','pub',40.6144,22.9598,'08:00:00','02:00:00',_binary '',356),(4330,'Οδυσσέας','restaurant',40.6369,22.9534,'12:30:00','18:00:00',_binary '',24788),(6211,'Pulp','bar',40.6321,22.9479,'09:00:00','23:59:00',_binary '',NULL),(7365,'McDonald\'s','fast_food',40.5745,22.9715,'12:00:00','23:59:00',_binary '',NULL),(8132,'Γιαννούλα','restaurant',40.64,22.9468,'13:30:00','01:00:00',_binary '',1566);
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:53:56

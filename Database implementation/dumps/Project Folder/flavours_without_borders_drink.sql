CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drink`
--

DROP TABLE IF EXISTS `drink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drink` (
  `drink_id` int(11) NOT NULL,
  `drink_name` varchar(500) NOT NULL,
  `drink_description` varchar(700) DEFAULT NULL,
  `drink_has_alcohol` bit(1) NOT NULL,
  `drink_is_approved` bit(1) DEFAULT b'0',
  `restaurant_id` int(11) NOT NULL,
  PRIMARY KEY (`drink_id`),
  UNIQUE KEY `drink_id_UNIQUE` (`drink_id`),
  KEY `food_is_offered_by_restaurant_id_idx` (`restaurant_id`),
  CONSTRAINT `drink_is_offered_by_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`restaurant_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drink`
--

LOCK TABLES `drink` WRITE;
/*!40000 ALTER TABLE `drink` DISABLE KEYS */;
INSERT INTO `drink` VALUES (3,'Κόκκινη πλατεία','Απολαυστικό τσάι με ιβίσκο',_binary '\0',_binary '',768),(25,'Λεμονάδα',NULL,_binary '\0',_binary '\0',4330),(46,'Long Island',NULL,_binary '',_binary '',768),(69,'Πορτοκαλάδα',NULL,_binary '\0',_binary '',9),(494,'La Trappe Quadrupel','Μερικοί υποστηρίζουν ότι είναι η καλύτερη μοναστηριακή μπύρα στον κόσμο',_binary '',_binary '',683),(984,'Long Island','Το πιο βαρή coctail που υπάρχει',_binary '',_binary '',1356),(3214,'Mojito','Δροσιστικό κουβανέζικο κοκτέιλ',_binary '',_binary '',1356),(3568,'Τσάι του βουνού','Καλό για τα λαιμά',_binary '\0',_binary '\0',236),(5673,'Ρετσίνα Μαλαματίνα','ΜΠΑΟΚ ΡΕ',_binary '',_binary '\0',8132),(6363,'White Russian','Διάσημο coctail που έπινε ο Jeff Bridges στη ταινία \"The big Lebowski\"',_binary '',_binary '',6211),(26427,'Old Fashioned','Κοκτέιλ με αμερικάνικες ρίζες',_binary '',_binary '',6211);
/*!40000 ALTER TABLE `drink` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:53:51

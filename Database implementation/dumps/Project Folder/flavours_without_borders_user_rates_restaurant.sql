CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_rates_restaurant`
--

DROP TABLE IF EXISTS `user_rates_restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_rates_restaurant` (
  `user_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `diet_id` int(11) DEFAULT NULL,
  `rating_grade` int(11) NOT NULL,
  `rating_date` date NOT NULL,
  `rating_text` varchar(700) DEFAULT NULL,
  `rating_accessibility` enum('easy','moderate','hard') DEFAULT NULL,
  PRIMARY KEY (`user_id`,`restaurant_id`),
  KEY `user_rates_restaurant_id_idx` (`restaurant_id`),
  KEY `user_rates_restaurant_with_diet_id_idx` (`diet_id`),
  CONSTRAINT `user_rates_restaurant_has_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_rates_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`restaurant_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_rates_restaurant_with_diet_id` FOREIGN KEY (`diet_id`) REFERENCES `diet` (`diet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rates_restaurant`
--

LOCK TABLES `user_rates_restaurant` WRITE;
/*!40000 ALTER TABLE `user_rates_restaurant` DISABLE KEYS */;
INSERT INTO `user_rates_restaurant` VALUES (7,15,2,2,'2017-06-01','Χάλι μαύρο',NULL),(7,7365,NULL,3,'2018-02-21','Μέτριο.',NULL),(9,15,NULL,3,'2016-05-08','Δεν χόρτασα','easy'),(215,4330,18,5,'2018-10-02','Χορταστικό φαΐ!','hard'),(215,6211,NULL,4,'2018-04-04','','hard'),(356,4330,NULL,4,'2018-09-16',NULL,NULL),(666,7365,NULL,3,'2018-04-16',NULL,'easy'),(8756,15,18,2,'2017-09-11',NULL,'moderate'),(24788,1356,NULL,5,'2018-06-08','Φιλικότατο προσωπικό!','moderate'),(24788,4330,2,3,'2018-01-04','Λίγες επιλογές για διαβητικούς.',NULL);
/*!40000 ALTER TABLE `user_rates_restaurant` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:53:57

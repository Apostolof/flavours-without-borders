SELECT
    `restaurant`.`restaurant_id` AS `restaurant_id`,
    `restaurant`.`restaurant_name` AS `restaurant_name`,
    `restaurant`.`restaurant_category` AS `restaurant_category`,
    `restaurant`.`restaurant_longitude` AS `restaurant_longitude`,
    `restaurant`.`restaurant_latitude` AS `restaurant_latitude`,
    `restaurant`.`restaurant_opening` AS `restaurant_opening`,
    `restaurant`.`restaurant_closing` AS `restaurant_closing`,
    `restaurant`.`user_id` AS `user_id`,
    NULL AS `food_id`,
    NULL AS `food_name`,
    NULL AS `food_description`,
    NULL AS `food_calories`,
    NULL AS `drink_id`,
    NULL AS `drink_name`,
    NULL AS `drink_description`,
    NULL AS `ingredient_name`,
    NULL AS `ingredient_has_alcohol`
FROM `restaurant`
WHERE (`restaurant`.`restaurant_is_approved` = FALSE) 
UNION
SELECT
    `restaurant`.`restaurant_id` AS `restaurant_id`,
    `restaurant`.`restaurant_name` AS `restaurant_name`,
    `restaurant`.`restaurant_category` AS `restaurant_category`,
    `restaurant`.`restaurant_longitude` AS `restaurant_longitude`,
    `restaurant`.`restaurant_latitude` AS `restaurant_latitude`,
    `restaurant`.`restaurant_opening` AS `restaurant_opening`,
    `restaurant`.`restaurant_closing` AS `restaurant_closing`,
    `restaurant`.`user_id` AS `user_id`,
    `food`.`food_id` AS `food_id`,
    `food`.`food_name` AS `food_name`,
    `food`.`food_description` AS `food_description`,
    `food`.`food_calories` AS `food_calories`,
    NULL AS `drink_id`,
    NULL AS `drink_name`,
    NULL AS `drink_description`,
    `ingredient`.`ingredient_name` AS `ingredient_name`,
    `ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol`
FROM `restaurant`
    LEFT JOIN `food`
        ON `restaurant`.`restaurant_id` = `food`.`restaurant_id`
    LEFT JOIN `food_has_ingredient`
        ON `food`.`food_id` = `food_has_ingredient`.`food_id`
    LEFT JOIN `ingredient`
        ON `food_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`
WHERE `food`.`food_is_approved` = FALSE
UNION
SELECT
    `restaurant`.`restaurant_id` AS `restaurant_id`,
    `restaurant`.`restaurant_name` AS `restaurant_name`,
    `restaurant`.`restaurant_category` AS `restaurant_category`,
    `restaurant`.`restaurant_longitude` AS `restaurant_longitude`,
    `restaurant`.`restaurant_latitude` AS `restaurant_latitude`,
    `restaurant`.`restaurant_opening` AS `restaurant_opening`,
    `restaurant`.`restaurant_closing` AS `restaurant_closing`,
    `restaurant`.`user_id` AS `user_id`,
    NULL AS `food_id`,
    NULL AS `food_name`,
    NULL AS `food_description`,
    NULL AS `food_calories`,
    `drink`.`drink_id` AS `drink_id`,
    `drink`.`drink_name` AS `drink_name`,
    `drink`.`drink_description` AS `drink_description`,
    `ingredient`.`ingredient_name` AS `ingredient_name`,
    `ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol`
FROM `restaurant`
    LEFT JOIN `drink`
        ON `restaurant`.`restaurant_id` = `drink`.`restaurant_id`
    LEFT JOIN `drink_has_ingredient`
        ON `drink`.`drink_id` = `drink_has_ingredient`.`drink_id`
    LEFT JOIN `ingredient`
        ON `drink_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`
WHERE `drink`.`drink_is_approved` = FALSE
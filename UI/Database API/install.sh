#!/bin/bash

# If /root/.my.cnf exists then it won't ask for root password
if [ -f /root/.my.cnf ]; then
	echo "Creating new Flavours database..."
	# mysql -e "CREATE DATABASE  IF NOT EXISTS flavoursWithoutBorders /*!40100 DEFAULT CHARACTER SET utf8 */;"
	mysql < schema.sql
	echo "Database successfully created!"
	echo ""
	echo "Please enter a password for the new Flavours database user!"
	read userpass
	echo ""
	echo "Creating new user..."
	mysql -e "DROP USER  IF EXISTS 'flavoursUser'@'localhost';"
	mysql -e "DROP USER  IF EXISTS 'flavoursUser'@'%';"
	mysql -e "CREATE USER 'flavoursUser'@'localhost' IDENTIFIED BY '${userpass}';"
	mysql -e "CREATE USER 'flavoursUser'@'%' IDENTIFIED BY '${userpass}';"
	echo "User successfully created!"
	echo ""
	echo "Granting ALL privileges on flavoursWithoutBorders to flavoursUser!"
	mysql -e "GRANT ALL ON flavoursWithoutBorders.* TO 'flavoursUser'@'localhost';"
	mysql -e "GRANT ALL ON flavoursWithoutBorders.* TO 'flavoursUser'@'%';"
	mysql -e "FLUSH PRIVILEGES;"
# If /root/.my.cnf doesn't exist then it'll ask for root password
else
	echo "Please enter root user MySQL password!"
	read rootpasswd
	echo "Creating new Flavours database..."
	# mysql -uroot -p${rootpasswd} -e "CREATE DATABASE  IF NOT EXISTS flavoursWithoutBorders /*!40100 DEFAULT CHARACTER SET utf8 */;"
	mysql -uroot -p${rootpasswd} < schema.sql
	echo "Database successfully created!"
	echo ""
	echo "Please enter a password for the new Flavours database user!"
	read userpass
	echo ""
	echo "Creating new user..."
	mysql -uroot -p${rootpasswd} -e "DROP USER  IF EXISTS 'flavoursUser'@'localhost';"
	mysql -uroot -p${rootpasswd} -e "DROP USER  IF EXISTS 'flavoursUser'@'%';"
	mysql -uroot -p${rootpasswd} -e "CREATE USER 'flavoursUser'@'localhost' IDENTIFIED BY '${userpass}';"
	mysql -uroot -p${rootpasswd} -e "CREATE USER 'flavoursUser'@'%' IDENTIFIED BY '${userpass}';"
	echo "User successfully created!"
	echo ""
	echo "Granting ALL privileges on flavoursWithoutBorders to flavoursUser!"
	mysql -uroot -p${rootpasswd} -e "GRANT ALL ON flavoursWithoutBorders.* TO 'flavoursUser'@'localhost';"
	mysql -uroot -p${rootpasswd} -e "GRANT ALL ON flavoursWithoutBorders.* TO 'flavoursUser'@'%';"
	mysql -uroot -p${rootpasswd} -e "FLUSH PRIVILEGES;"
fi

echo ""
cd hyrieus/

echo "Creating virtual environment..."
virtualenv -p python3 env
source ./env/bin/activate
echo "Environment successfully created!"

echo ""
echo "Installing required modules..."
[ -r "../requirements.txt" ] && pip3 install -r "../requirements.txt"
echo "Modules installed!"

echo ""
echo Input the server IP to use:
read ip_to_use

sed -i "s/server_ip_goes_here/$ip_to_use/g" flavoursAPI/settings.py
sed -i "s/database_password_goes_here/$userpass/g" flavoursAPI/settings.py

echo ""
echo "Making migrations..."
python3 manage.py makemigrations hyrieus
python3 manage.py migrate
mysql -u flavoursUser -p${userpass} < ../create_roles.sql
echo "Done!"

echo ""
echo "Creating API superuser..."
python3 manage.py createsuperuser

echo ""
echo "Starting server..."
python3 manage.py runserver "$ip_to_use:8181"
echo ""

exit

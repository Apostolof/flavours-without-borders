package gr.auth.databases.flavours.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DietRatingPair implements Parcelable {
    private String dietName;
    private double rating;

    public DietRatingPair(String dietName, double rating) {
        this.dietName = dietName;
        this.rating = rating;
    }

    public String getDietName() {
        return dietName;
    }

    public double getRating() {
        return rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(dietName);
        out.writeDouble(rating);
    }

    public static final Parcelable.Creator<DietRatingPair> CREATOR = new Parcelable.Creator<DietRatingPair>() {
        public DietRatingPair createFromParcel(Parcel in) {
            return new DietRatingPair(in);
        }

        public DietRatingPair[] newArray(int size) {
            return new DietRatingPair[size];
        }
    };

    private DietRatingPair(Parcel in) {
        dietName = in.readString();
        rating = in.readDouble();
    }
}

package gr.auth.databases.flavours.activities.restaurant.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.profile.ProfileActivity;
import gr.auth.databases.flavours.model.RestaurantRating;
import gr.auth.databases.flavours.utils.RestaurantRatingsAdapter;

import static gr.auth.databases.flavours.activities.profile.ProfileActivity.BUNDLE_USER_ID;
import static gr.auth.databases.flavours.activities.profile.ProfileActivity.BUNDLE_USER_NAME;

public class RestaurantRatingsFragment extends Fragment implements
        RestaurantRatingsAdapter.ProfileRestaurantRatingsAdapterInteractionListener {

    public RestaurantRatingsFragment() {
        // Required empty public constructor
    }

    private static final String RESTAURANT_RATINGS = "RESTAURANT_RATINGS";

    private ArrayList<RestaurantRating> ratings;

    public static RestaurantRatingsFragment newInstance(ArrayList<RestaurantRating> ratings) {
        RestaurantRatingsFragment fragment = new RestaurantRatingsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(RESTAURANT_RATINGS, ratings);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        ratings = getArguments().getParcelableArrayList(RESTAURANT_RATINGS);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.reusable_recycler_list, container, false);
        Context context = getContext();
        assert context != null;
        RestaurantRatingsAdapter itemAdapter = new RestaurantRatingsAdapter(context, ratings, this);
        RecyclerView mainContent = rootView.findViewById(R.id.recycler_list);
        mainContent.setAdapter(itemAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mainContent.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainContent.getContext(),
                layoutManager.getOrientation());
        mainContent.addItemDecoration(dividerItemDecoration);

        return rootView;
    }

    @Override
    public void onProfileRestaurantRatingsAdapterInteraction(int profileID, String profileUsername) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        Bundle extras = new Bundle();
        extras.putInt(BUNDLE_USER_ID, profileID);
        extras.putString(BUNDLE_USER_NAME, profileUsername);
        intent.putExtras(extras);
        startActivity(intent);
    }
}

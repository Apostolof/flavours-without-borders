package gr.auth.databases.flavours.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Profile implements Parcelable {
    private int id, age, reviewsNumber;
    private String username, email;
    private ArrayList<RestaurantSummary> restaurantsOwned;
    private ArrayList<DietSummary> dietsFollowed;
    private ArrayList<IngredientSummary> ingredientsProhibited;

    public Profile(int id, int age, int reviewsNumber, String username, String email,
                   ArrayList<RestaurantSummary> restaurantsOwned, ArrayList<DietSummary> dietsFollowed,
                   ArrayList<IngredientSummary> ingredientsProhibited) {
        this.id = id;
        this.age = age;
        this.reviewsNumber = reviewsNumber;
        this.username = username;
        this.email = email;
        this.restaurantsOwned = restaurantsOwned;
        this.dietsFollowed = dietsFollowed;
        this.ingredientsProhibited = ingredientsProhibited;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public int getReviewsNumber() {
        return reviewsNumber;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public ArrayList<RestaurantSummary> getRestaurantsOwned() {
        return restaurantsOwned;
    }

    public ArrayList<DietSummary> getDietsFollowed() {
        return dietsFollowed;
    }

    public ArrayList<IngredientSummary> getIngredientsProhibited() {
        return ingredientsProhibited;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeInt(age);
        out.writeInt(reviewsNumber);
        out.writeString(username);
        out.writeString(email);
        out.writeList(restaurantsOwned);
        out.writeList(dietsFollowed);
        out.writeList(ingredientsProhibited);
    }

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    private Profile(Parcel in) {
        id = in.readInt();
        age = in.readInt();
        reviewsNumber = in.readInt();
        username = in.readString();
        email = in.readString();
        restaurantsOwned = (ArrayList<RestaurantSummary>) in.readArrayList(DietRatingPair.class.getClassLoader());
        dietsFollowed = (ArrayList<DietSummary>) in.readArrayList(DietRatingPair.class.getClassLoader());
        ingredientsProhibited = (ArrayList<IngredientSummary>) in.readArrayList(DietRatingPair.class.getClassLoader());
    }
}

package gr.auth.databases.flavours.activities;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.Restaurant;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static gr.auth.databases.flavours.session.SessionManager.restaurantsUrl;

public class AddRestaurantActivity extends BaseActivity implements AdapterView.OnItemSelectedListener,
        TimePickerDialog.OnTimeSetListener, OnMapReadyCallback {
    private MapView gMapView = null;
    private EditText workingHours;
    private String restaurantType;

    private LatLng Thessaloniki = new LatLng(40.628477, 22.952040);
    private MarkerOptions selectLocationMarker = new MarkerOptions().position(Thessaloniki)
            .title("Restaurant location")
            .snippet("Please move the marker if needed.")
            .draggable(true);
    private double markerLogitude, markerLatitude;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_restaurant);

        Toolbar toolbar = findViewById(R.id.add_restaurant_toolbar);
        toolbar.setTitle(getString(R.string.add_restaurant_toolbar_title));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        final EditText restaurantName = findViewById(R.id.add_restaurant_name);

        Spinner spinner = findViewById(R.id.add_restaurant_type);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.restaurant_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        gMapView = findViewById(R.id.add_restaurant_map);
        gMapView.onCreate(savedInstanceState);
        gMapView.getMapAsync(this);

        workingHours = findViewById(R.id.add_restaurant_working_hours);
        workingHours.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog tpd;

                    String timeSet = workingHours.getText().toString();
                    if (!timeSet.isEmpty()) {
                        int hourStart, minuteStart, hourEnd, minuteEnd;
                        String[] startEnd = timeSet.split("-");

                        hourStart = Integer.parseInt(startEnd[0].split(":")[0].trim());
                        minuteStart = Integer.parseInt(startEnd[0].split(":")[1].trim());
                        hourEnd = Integer.parseInt(startEnd[1].split(":")[0].trim());
                        minuteEnd = Integer.parseInt(startEnd[1].split(":")[1].trim());

                        tpd = TimePickerDialog.newInstance(AddRestaurantActivity.this,
                                hourStart, minuteStart, true, hourEnd, minuteEnd);
                    } else {
                        tpd = TimePickerDialog.newInstance(AddRestaurantActivity.this,
                                now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
                    }

                    tpd.show(getFragmentManager(), "Timepickerdialog");
                }
                return false;
            }
        });

        findViewById(R.id.add_restaurant_add_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (restaurantType == null || restaurantType.isEmpty()) {
                    Toast.makeText(view.getContext(), "Please select a restaurant type first.",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                String hours = workingHours.getText().toString();

                AddRestaurantTask addRestaurantTask = new AddRestaurantTask();
                addRestaurantTask.execute(restaurantName.getText().toString(),
                        restaurantType.toLowerCase(),
                        "" + markerLogitude, "" + markerLatitude,
                        hours.isEmpty() ? "" : hours.split("-")[0].trim(),
                        hours.isEmpty() ? "" : hours.split("-")[1].trim());
                finish();
            }
        });

        createDrawer();
        drawer.setSelection(-1);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        restaurantType = Restaurant.RestaurantType.getWorkingTypeFromId(pos);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String hourStringEnd = hourOfDayEnd < 10 ? "0" + hourOfDayEnd : "" + hourOfDayEnd;
        String minuteStringEnd = minuteEnd < 10 ? "0" + minuteEnd : "" + minuteEnd;

        workingHours.setText(getString(R.string.add_restaurant_working_hours_placeholder,
                hourString, minuteString, hourStringEnd, minuteStringEnd));
    }

    @Override
    public void onResume() {
        super.onResume();

        if (gMapView != null) {
            gMapView.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (gMapView != null) {
            gMapView.onDestroy();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (gMapView != null) {
            gMapView.onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (gMapView != null) {
            gMapView.onStop();
        }

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (gMapView != null) {
            gMapView.onSaveInstanceState(outState);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(selectLocationMarker);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Thessaloniki, 12.0f));
        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                markerLogitude = marker.getPosition().longitude;
                markerLatitude = marker.getPosition().latitude;
            }
        });
    }

    private class AddRestaurantTask extends AsyncTask<String, Void, Integer> {
        private static final String REQ_RESTAURANT_NAME = "restaurant_name";
        private static final String REQ_RESTAURANT_CATEGORY = "restaurant_category";
        private static final String REQ_RESTAURANT_LONGITUDE = "restaurant_longitude";
        private static final String REQ_RESTAURANT_LATITUDE = "restaurant_latitude";
        private static final String REQ_RESTAURANT_OPENING = "restaurant_opening";
        private static final String REQ_RESTAURANT_CLOSING = "restaurant_closing";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(String... params) {
            //Builds the request
            RequestBody formBody = new FormBody.Builder()
                    .add(REQ_RESTAURANT_NAME, params[0])
                    .add(REQ_RESTAURANT_CATEGORY, params[1])
                    .add(REQ_RESTAURANT_LONGITUDE, params[2])
                    .add(REQ_RESTAURANT_LATITUDE, params[3])
                    .add(REQ_RESTAURANT_OPENING, params[4])
                    .add(REQ_RESTAURANT_CLOSING, params[5])
                    .build();
            Request request = new Request.Builder()
                    .url(restaurantsUrl)
                    .post(formBody)
                    .build();

            try {
                Response response = client.newCall(request).execute();

                if (response.code() == 201) {
                    Toast.makeText(AddRestaurantActivity.this,
                            "Restaurant was added!", Toast.LENGTH_SHORT).show();
                    return 0;
                }
                return 1;
            } catch (Exception e) {
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
        }
    }
}

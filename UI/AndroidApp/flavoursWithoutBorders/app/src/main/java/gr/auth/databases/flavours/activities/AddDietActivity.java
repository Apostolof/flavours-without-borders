package gr.auth.databases.flavours.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.Toolbar;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.Ingredient;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

import static gr.auth.databases.flavours.session.SessionManager.addDietUrl;

public class AddDietActivity extends BaseActivity {
    private ArrayList<Ingredient> ingredients = new ArrayList<>();
    private EditText dietNameInput, dietDescriptionInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_diet);

        Toolbar toolbar = findViewById(R.id.add_diet_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        dietNameInput = findViewById(R.id.add_diet_name);
        dietDescriptionInput = findViewById(R.id.add_diet_description);
        AppCompatButton addDietButton = findViewById(R.id.add_diet_add_btn);
        final TextView ingredientsTitle = findViewById(R.id.add_diet_add_ingredient_title);
        final EditText addIngredientInput = findViewById(R.id.add_diet_add_ingredient);
        final LinearLayout ingredientsList = findViewById(R.id.add_diet_ingredients_list);

        addIngredientInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    final Ingredient newIngredient = new Ingredient(addIngredientInput.getText().toString(), false);
                    ingredients.add(newIngredient);

                    View ingredientRow = getLayoutInflater().inflate(R.layout.add_item_ingredient_row, ingredientsList, false);
                    TextView ingredientName = ingredientRow.findViewById(R.id.add_item_ingredient_name);
                    ingredientName.setText(ingredients.get(ingredients.size() - 1).getName());

                    ingredientRow.findViewById(R.id.add_item_ingredient_has_alcohol).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ingredients.get(ingredients.indexOf(newIngredient)).
                                    setHasAlcohol(!ingredients.get(ingredients.indexOf(newIngredient)).hasAlcohol());
                        }
                    });

                    AppCompatImageButton removeIngredientBtn = ingredientRow.findViewById(R.id.add_item_remove_ingredient);
                    removeIngredientBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int index = ingredientsList.indexOfChild((View) view.getParent());
                            ingredients.remove(index);
                            ingredientsList.removeViewAt(index);
                        }
                    });

                    ingredientsList.addView(ingredientRow);
                    ingredientsTitle.setVisibility(View.VISIBLE);
                    ingredientsList.setVisibility(View.VISIBLE);

                    addIngredientInput.setText("");
                    return true;
                }
                return false;
            }
        });

        addDietButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddDietTask addDietTask = new AddDietTask();
                addDietTask.execute();
            }
        });

        createDrawer();
        drawer.setSelection(-1);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AddDietTask extends AsyncTask<Void, Void, Integer> {
        private static final String REQ_DIET_NAME = "diet_name";
        private static final String REQ_DIET_DESCRIPTION = "diet_description";

        private static final String REQ_DIET_PROHIBITS_INGREDIENTS_LIST = "ingredients";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            StringBuilder ingredientsJson = new StringBuilder("[");
            for (int ingredientIndex = 0; ingredientIndex < ingredients.size(); ++ingredientIndex) {
                Ingredient ingredient = ingredients.get(ingredientIndex);
                ingredientsJson.append("{\"ingredient_name\": \"").append(ingredient.getName())
                        .append("\", \"ingredient_has_alcohol\": \"").append(ingredient.hasAlcohol())
                        .append("\"}");
                if (ingredientIndex < ingredients.size() - 1) {
                    ingredientsJson.append(",");
                }
            }
            ingredientsJson.append("]");

            //Builds the request
            RequestBody formBody = new FormBody.Builder()
                    .add(REQ_DIET_NAME, dietNameInput.getText().toString())
                    .add(REQ_DIET_DESCRIPTION, dietDescriptionInput.getText().toString())
                    .add(REQ_DIET_PROHIBITS_INGREDIENTS_LIST, ingredientsJson.toString())
                    .build();
            Request request = new Request.Builder()
                    .url(addDietUrl)
                    .post(formBody)
                    .build();

            try {
                client.newCall(request).execute();
                return 0;
            } catch (Exception e) {
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            Toast.makeText(AddDietActivity.this,
                    "Diet was added!", Toast.LENGTH_SHORT).show();
            AddDietActivity.this.finish();
        }
    }
}
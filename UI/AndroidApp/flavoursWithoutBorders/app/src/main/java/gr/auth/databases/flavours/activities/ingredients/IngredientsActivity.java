package gr.auth.databases.flavours.activities.ingredients;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.AddIngredientActivity;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.Ingredient;
import gr.auth.databases.flavours.utils.ProhibitIngredientStateTask;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static gr.auth.databases.flavours.activities.AddIngredientActivity.INGREDIENT_ADD_RESULT;
import static gr.auth.databases.flavours.session.SessionManager.ingredientsUserViewUrl;

public class IngredientsActivity extends BaseActivity
        implements IngredientsAdapter.SubscribeIngredientsAdapterInteractionListener,
        IngredientsAdapter.IngredientsAdapterInteractionListener {
    public static final String INGREDIENT_PICK_RESULT = "INGREDIENT_PICK_RESULT";
    private static final int ADD_NEW_INGREDIENT_REQUEST = 2000;

    private RecyclerView recyclerView;
    private ArrayList<String> userProhibitsIngredients;
    private boolean isCalledForResult;
    private ArrayList<Ingredient> ingredients = new ArrayList<>();
    private IngredientsAdapter ingredientsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients);

        isCalledForResult = getCallingActivity() != null;

        Toolbar toolbar = findViewById(R.id.ingredients_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        createDrawer();

        FloatingActionButton FAB = findViewById(R.id.ingredients_fab);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), AddIngredientActivity.class);
                startActivityForResult(intent, ADD_NEW_INGREDIENT_REQUEST);
            }
        });

        recyclerView = findViewById(R.id.ingredients_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        IngredientsTask ingredientsTask = new IngredientsTask();
        ingredientsTask.execute();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (!isCalledForResult && id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        if (isCalledForResult) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }

        super.onDestroy();
    }

    @Override
    public void onSubscribeIngredientsAdapterInteraction(Ingredient ingredient) {
        ProhibitIngredientStateTask prohibitIngredientStateTask = new ProhibitIngredientStateTask(client);
        prohibitIngredientStateTask.execute(ingredient.getName());
    }

    @Override
    public void onIngredientsAdapterInteraction(Ingredient ingredient) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(INGREDIENT_PICK_RESULT, ingredient);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_NEW_INGREDIENT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Ingredient result = data.getParcelableExtra(INGREDIENT_ADD_RESULT);
                ingredients.add(0, result);
                ingredientsAdapter.notifyDataSetChanged();
                Toast.makeText(this, "New ingredient added.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class IngredientsTask extends AsyncTask<Void, Void, Integer> {
        private static final String JSON_TAG_INGREDIENTS_LIST = "ingredients";
        private static final String JSON_TAG_INGREDIENT_NAME = "ingredient_name";
        private static final String JSON_TAG_INGREDIENT_HAS_ALCOHOL = "ingredient_has_alcohol";

        private static final String JSON_TAG_USER_PROHIBITS_INGREDIENT_LIST = "prohibits";
        private static final String JSON_TAG_USER_PROHIBITS_INGREDIENT = "ingredient_name";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            //Builds the request
            Request request = new Request.Builder()
                    .url(ingredientsUserViewUrl)
                    .build();

            try {
                //Makes request & handles response
                Response response = client.newCall(request).execute();

                ResponseBody responseBody = response.body();
                assert responseBody != null;
                String result = responseBody.string();
                JSONObject jsonResponse = new JSONObject(result);

                userProhibitsIngredients = new ArrayList<>();
                JSONArray jsonUserProhibitsIngredients = jsonResponse.getJSONArray(JSON_TAG_USER_PROHIBITS_INGREDIENT_LIST);
                for (int ingredientIndex = 0; ingredientIndex < jsonUserProhibitsIngredients.length(); ++ingredientIndex) {
                    JSONObject ingredient = jsonUserProhibitsIngredients.getJSONObject(ingredientIndex);
                    userProhibitsIngredients.add(ingredient.getString(JSON_TAG_USER_PROHIBITS_INGREDIENT));
                }

                JSONArray jsonIngredientsList = jsonResponse.getJSONArray(JSON_TAG_INGREDIENTS_LIST);
                for (int dietIndex = 0; dietIndex < jsonIngredientsList.length(); ++dietIndex) {
                    JSONObject ingredient = jsonIngredientsList.getJSONObject(dietIndex);
                    ingredients.add(new Ingredient(ingredient.getString(JSON_TAG_INGREDIENT_NAME),
                            ingredient.getBoolean(JSON_TAG_INGREDIENT_HAS_ALCOHOL)));
                }
                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (isCalledForResult) {
                ingredientsAdapter = new IngredientsAdapter(IngredientsActivity.this,
                        ingredients,
                        userProhibitsIngredients,
                        null,
                        IngredientsActivity.this);
            } else {
                ingredientsAdapter = new IngredientsAdapter(IngredientsActivity.this,
                        ingredients,
                        userProhibitsIngredients,
                        IngredientsActivity.this,
                        null);
            }
            recyclerView.setAdapter(ingredientsAdapter);
        }
    }
}

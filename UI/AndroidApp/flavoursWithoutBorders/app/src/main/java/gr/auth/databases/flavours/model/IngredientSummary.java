package gr.auth.databases.flavours.model;

public class IngredientSummary {
    private String name;

    public IngredientSummary(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
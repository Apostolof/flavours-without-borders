package gr.auth.databases.flavours.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.ItemRating;

public class ItemRatingsAdapter extends RecyclerView.Adapter<ItemRatingsAdapter.RatingViewHolder> {
    private Context context;
    private ArrayList<ItemRating> ratings;
    private ProfileItemRatingsAdapterInteractionListener profileItemRatingsAdapterInteractionListener;

    public ItemRatingsAdapter(@NonNull Context context, ArrayList<ItemRating> ratings,
                              ProfileItemRatingsAdapterInteractionListener profileItemRatingsAdapterInteractionListener) {
        this.context = context;
        this.ratings = ratings;
        this.profileItemRatingsAdapterInteractionListener = profileItemRatingsAdapterInteractionListener;
    }

    @NonNull
    @Override
    public ItemRatingsAdapter.RatingViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                  int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rating_row,
                parent, false);
        return new RatingViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RatingViewHolder holder, int position) {
        holder.authorUsername.setText(ratings.get(position).getUsername());
        if (profileItemRatingsAdapterInteractionListener != null) {
            holder.authorUsername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profileItemRatingsAdapterInteractionListener.
                            onProfileItemRatingsAdapterInteraction(
                                    ratings.get(holder.getAdapterPosition()).getUserID(),
                                    ratings.get(holder.getAdapterPosition()).getUsername());
                }
            });
        }
        holder.date.setText(ratings.get(position).getDate());

        holder.grade.setText(context.getString(R.string.item_ratings_row_grade_placeholder,
                ratings.get(position).getGrade()));

        if (ratings.get(position).getPortionSize() != null) {
            holder.portionSize.setVisibility(View.VISIBLE);
            holder.portionSize.setText(context.getString(R.string.item_ratings_row_accessibility_placeholder,
                    ratings.get(position).getPortionSize().toString()));
        } else {
            holder.portionSize.setVisibility(View.GONE);
        }

        if (!ratings.get(position).getText().equals("null")) {
            holder.text.setVisibility(View.VISIBLE);
            holder.text.setText(ratings.get(position).getText());
        } else {
            holder.text.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return ratings == null ? 0 : ratings.size();
    }

    static class RatingViewHolder extends RecyclerView.ViewHolder {
        TextView authorUsername, date, grade, portionSize, text;

        RatingViewHolder(View v) {
            super(v);
            authorUsername = v.findViewById(R.id.item__rating_author_username);
            date = v.findViewById(R.id.item__rating_date);
            grade = v.findViewById(R.id.item__rating_grade);
            portionSize = v.findViewById(R.id.item__rating_portion_size);
            text = v.findViewById(R.id.item__rating_text);
        }
    }

    public interface ProfileItemRatingsAdapterInteractionListener {
        void onProfileItemRatingsAdapterInteraction(int profileID, String profileUsername);
    }
}
package gr.auth.databases.flavours.activities.restaurant;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.AddItemActivity;
import gr.auth.databases.flavours.activities.RateRestaurantActivity;
import gr.auth.databases.flavours.activities.drink.DrinkActivity;
import gr.auth.databases.flavours.activities.food.FoodActivity;
import gr.auth.databases.flavours.activities.restaurant.fragments.RestaurantDrinksFragment;
import gr.auth.databases.flavours.activities.restaurant.fragments.RestaurantFoodsFragment;
import gr.auth.databases.flavours.activities.restaurant.fragments.RestaurantInfoFragment;
import gr.auth.databases.flavours.activities.restaurant.fragments.RestaurantRatingsFragment;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.DietRatingPair;
import gr.auth.databases.flavours.model.ItemSummary;
import gr.auth.databases.flavours.model.Restaurant;
import gr.auth.databases.flavours.model.RestaurantRating;
import gr.auth.databases.flavours.model.RestaurantUserView;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static gr.auth.databases.flavours.activities.AddItemActivity.BUNDLE_ADD_ITEM_ITEM_RESTAURANT_ID;
import static gr.auth.databases.flavours.activities.AddItemActivity.BUNDLE_ADD_ITEM_ITEM_TYPE;
import static gr.auth.databases.flavours.activities.RateRestaurantActivity.BUNDLE_RATE_RESTAURANT;
import static gr.auth.databases.flavours.activities.drink.DrinkActivity.BUNDLE_ARG_DRINK;
import static gr.auth.databases.flavours.activities.food.FoodActivity.BUNDLE_ARG_FOOD;
import static gr.auth.databases.flavours.session.SessionManager.restaurantsUserViewUrl;

public class RestaurantActivity extends BaseActivity
        implements RestaurantFoodsFragment.RestaurantFoodsFragmentInteractionListener,
        RestaurantDrinksFragment.RestaurantDrinksFragmentInteractionListener {
    public static final String BUNDLE_ARG_RESTAURANT = "BUNDLE_ARG_RESTAURANT";

    private Restaurant mRestaurant;
    private RestaurantUserView restaurant;
    private ArrayList<ItemSummary> foods = new ArrayList<>();
    private ArrayList<ItemSummary> drinks = new ArrayList<>();
    private ArrayList<RestaurantRating> ratings = new ArrayList<>();

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private FloatingActionButton FAB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        mRestaurant = extras.getParcelable(BUNDLE_ARG_RESTAURANT);

        Toolbar toolbar = findViewById(R.id.restaurant_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(mRestaurant.getName());
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        createDrawer();
        drawer.setSelection(-1);

        FAB = findViewById(R.id.restaurant_fab);
        FAB.hide();
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() == 1) {
                    Intent intent = new Intent(view.getContext(), AddItemActivity.class);
                    intent.putExtra(BUNDLE_ADD_ITEM_ITEM_TYPE, ItemSummary.ItemType.FOOD);
                    intent.putExtra(BUNDLE_ADD_ITEM_ITEM_RESTAURANT_ID, mRestaurant.getId());
                    startActivity(intent);
                } else if (viewPager.getCurrentItem() == 2) {
                    Intent intent = new Intent(view.getContext(), AddItemActivity.class);
                    intent.putExtra(BUNDLE_ADD_ITEM_ITEM_TYPE, ItemSummary.ItemType.DRINK);
                    intent.putExtra(BUNDLE_ADD_ITEM_ITEM_RESTAURANT_ID, mRestaurant.getId());
                    startActivity(intent);
                } else if (viewPager.getCurrentItem() == 3) {
                    Intent intent = new Intent(view.getContext(), RateRestaurantActivity.class);
                    intent.putExtra(BUNDLE_RATE_RESTAURANT, mRestaurant);
                    startActivity(intent);
                }
            }
        });

        viewPager = findViewById(R.id.restaurant_pager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (position == 0) {
                    FAB.hide();
                } else {
                    FAB.show();
                }
            }
        });

        tabLayout = findViewById(R.id.restaurant_tabs);

        RestaurantTask restaurantTask = new RestaurantTask();
        restaurantTask.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRestaurantFoodsFragmentInteraction(ItemSummary item) {
        Intent intent = new Intent(this, FoodActivity.class);
        intent.putExtra(BUNDLE_ARG_FOOD, item);
        startActivity(intent);
    }

    @Override
    public void onRestaurantDrinksFragmentInteraction(ItemSummary item) {
        Intent intent = new Intent(this, DrinkActivity.class);
        intent.putExtra(BUNDLE_ARG_DRINK, item);
        startActivity(intent);
    }

    private void setupViewPager(ViewPager viewPager) {
        RestaurantPagerAdapter adapter = new RestaurantPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(RestaurantInfoFragment.newInstance(restaurant), "INFO");
        adapter.addFrag(RestaurantFoodsFragment.newInstance(foods), "FOOD");
        adapter.addFrag(RestaurantDrinksFragment.newInstance(drinks), "DRINKS");
        adapter.addFrag(RestaurantRatingsFragment.newInstance(ratings), "RATINGS");
        viewPager.setAdapter(adapter);
    }

    private class RestaurantPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        RestaurantPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private class RestaurantTask extends AsyncTask<Void, Void, Integer> {
        private static final String JSON_TAG_RESTAURANT_INFO = "restaurantInfo";
        private static final String JSON_TAG_RESTAURANT_ID = "restaurant_id";
        private static final String JSON_TAG_RESTAURANT_NAME = "restaurant_name";
        private static final String JSON_TAG_RESTAURANT_CATEGORY = "restaurant_category";
        private static final String JSON_TAG_RESTAURANT_LONGITUDE = "restaurant_longitude";
        private static final String JSON_TAG_RESTAURANT_LATITUDE = "restaurant_latitude";
        private static final String JSON_TAG_RESTAURANT_OPENING = "restaurant_opening";
        private static final String JSON_TAG_RESTAURANT_CLOSING = "restaurant_closing";
        private static final String JSON_TAG_RESTAURANT_IS_ACCEPTED = "restaurant_is_approved";
        private static final String JSON_TAG_RESTAURANT_AVG_RATING_OBJ = "averageRating";
        private static final String JSON_TAG_RESTAURANT_AVG_RATING = "rating_grade__avg";

        private static final String JSON_TAG_RESTAURANT_RATINGS_PER_DIET = "avgRatingPerDiet";
        private static final String JSON_TAG_RESTAURANT_RATINGS_PER_DIET_NAME = "diet_name";
        private static final String JSON_TAG_RESTAURANT_RATINGS_PER_DIET_RATING = "average_rating";

        private static final String JSON_TAG_RESTAURANT_FOODS = "foods";
        private static final String JSON_TAG_RESTAURANT_FOOD_ID = "food_id";
        private static final String JSON_TAG_RESTAURANT_FOOD_NAME = "food_name";
        private static final String JSON_TAG_RESTAURANT_FOOD_IS_ACCEPTED = "food_is_approved";

        private static final String JSON_TAG_RESTAURANT_DRINKS = "drinks";
        private static final String JSON_TAG_RESTAURANT_DRINK_ID = "drink_id";
        private static final String JSON_TAG_RESTAURANT_DRINK_NAME = "drink_name";
        private static final String JSON_TAG_RESTAURANT_DRINK_IS_ACCEPTED = "drink_is_approved";

        private static final String JSON_TAG_RESTAURANT_RATINGS = "ratings";
        private static final String JSON_TAG_RESTAURANT_RATING_GRADE = "rating_grade";
        private static final String JSON_TAG_RESTAURANT_RATING_USER_ID = "user_id";
        private static final String JSON_TAG_RESTAURANT_RATING_USERNAME = "username";
        private static final String JSON_TAG_RESTAURANT_RATING_TEXT = "rating_text";
        private static final String JSON_TAG_RESTAURANT_RATING_DATE = "rating_date";
        private static final String JSON_TAG_RESTAURANT_RATING_ACCESSIBILITY = "rating_accessibility";
        private static final String JSON_TAG_RESTAURANT_RATING_DIET = "diet_name";

        @Override
        protected void onPreExecute() {
            foods.clear();
            drinks.clear();
            ratings.clear();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            String requestUrl = restaurantsUserViewUrl + mRestaurant.getId() + "/";

            //Builds the request
            Request request = new Request.Builder()
                    .url(requestUrl)
                    .build();

            try {
                //Makes request & handles response
                Response response = client.newCall(request).execute();

                ResponseBody responseBody = response.body();
                assert responseBody != null;
                String result = responseBody.string();
                JSONObject jsonResponse = new JSONObject(result);

                JSONObject jsonRestaurantInfo = jsonResponse.getJSONObject(JSON_TAG_RESTAURANT_INFO);
                JSONArray jsonRatingsPerDiet = jsonResponse.getJSONArray(JSON_TAG_RESTAURANT_RATINGS_PER_DIET);
                ArrayList<DietRatingPair> ratingsPerDiet = new ArrayList<>();

                for (int dietIndex = 0; dietIndex < jsonRatingsPerDiet.length(); ++dietIndex) {
                    JSONObject dietAvgRating = (JSONObject) jsonRatingsPerDiet.get(dietIndex);
                    ratingsPerDiet.add(new DietRatingPair(dietAvgRating.getString(JSON_TAG_RESTAURANT_RATINGS_PER_DIET_NAME),
                            dietAvgRating.getDouble(JSON_TAG_RESTAURANT_RATINGS_PER_DIET_RATING)));
                }

                double avgRestaurantRating = -1;
                if (!jsonResponse.getJSONObject(JSON_TAG_RESTAURANT_AVG_RATING_OBJ).isNull(JSON_TAG_RESTAURANT_AVG_RATING)) {
                    avgRestaurantRating = jsonResponse.getJSONObject(JSON_TAG_RESTAURANT_AVG_RATING_OBJ).getDouble(JSON_TAG_RESTAURANT_AVG_RATING);
                }

                restaurant = new RestaurantUserView(jsonRestaurantInfo.getInt(JSON_TAG_RESTAURANT_ID),
                        jsonRestaurantInfo.getString(JSON_TAG_RESTAURANT_NAME),
                        jsonRestaurantInfo.getString(JSON_TAG_RESTAURANT_CATEGORY),
                        jsonRestaurantInfo.getDouble(JSON_TAG_RESTAURANT_LONGITUDE),
                        jsonRestaurantInfo.getDouble(JSON_TAG_RESTAURANT_LATITUDE),
                        jsonRestaurantInfo.getString(JSON_TAG_RESTAURANT_OPENING),
                        jsonRestaurantInfo.getString(JSON_TAG_RESTAURANT_CLOSING),
                        jsonRestaurantInfo.getBoolean(JSON_TAG_RESTAURANT_IS_ACCEPTED),
                        avgRestaurantRating,
                        ratingsPerDiet);

                JSONArray jsonFoods = jsonResponse.getJSONArray(JSON_TAG_RESTAURANT_FOODS);
                for (int foodIndex = 0; foodIndex < jsonFoods.length(); ++foodIndex) {
                    JSONObject food = jsonFoods.getJSONObject(foodIndex);
                    foods.add(new ItemSummary(food.getString(JSON_TAG_RESTAURANT_FOOD_NAME),
                            food.getInt(JSON_TAG_RESTAURANT_FOOD_ID),
                            ItemSummary.ItemType.FOOD,
                            food.getBoolean(JSON_TAG_RESTAURANT_FOOD_IS_ACCEPTED)));
                }

                JSONArray jsonDrinks = jsonResponse.getJSONArray(JSON_TAG_RESTAURANT_DRINKS);
                for (int drinkIndex = 0; drinkIndex < jsonDrinks.length(); ++drinkIndex) {
                    JSONObject drink = jsonDrinks.getJSONObject(drinkIndex);
                    drinks.add(new ItemSummary(drink.getString(JSON_TAG_RESTAURANT_DRINK_NAME),
                            drink.getInt(JSON_TAG_RESTAURANT_DRINK_ID),
                            ItemSummary.ItemType.DRINK,
                            drink.getBoolean(JSON_TAG_RESTAURANT_DRINK_IS_ACCEPTED)));
                }

                JSONArray jsonRatings = jsonResponse.getJSONArray(JSON_TAG_RESTAURANT_RATINGS);
                for (int ratingIndex = 0; ratingIndex < jsonRatings.length(); ++ratingIndex) {
                    JSONObject rating = jsonRatings.getJSONObject(ratingIndex);
                    ratings.add(new RestaurantRating(rating.getInt(JSON_TAG_RESTAURANT_RATING_GRADE),
                            rating.getInt(JSON_TAG_RESTAURANT_RATING_USER_ID),
                            rating.getString(JSON_TAG_RESTAURANT_RATING_USERNAME),
                            rating.getString(JSON_TAG_RESTAURANT_RATING_TEXT),
                            rating.getString(JSON_TAG_RESTAURANT_RATING_DATE),
                            RestaurantRating.Accessibility.getEnumTypeFromString(
                                    rating.getString(JSON_TAG_RESTAURANT_RATING_ACCESSIBILITY)),
                            rating.getString(JSON_TAG_RESTAURANT_RATING_DIET)));
                }

                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
        }
    }
}

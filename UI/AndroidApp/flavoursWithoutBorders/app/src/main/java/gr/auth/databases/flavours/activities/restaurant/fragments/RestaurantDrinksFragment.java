package gr.auth.databases.flavours.activities.restaurant.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseApplication;
import gr.auth.databases.flavours.base.BaseFragment;
import gr.auth.databases.flavours.model.ItemSummary;
import okhttp3.Request;
import okhttp3.RequestBody;

import static gr.auth.databases.flavours.session.SessionManager.acceptDrinkUrl;

public class RestaurantDrinksFragment extends BaseFragment implements
        RestaurantItemAdapter.AcceptItemAdapterInteractionListener {

    public RestaurantDrinksFragment() {
        // Required empty public constructor
    }

    private static final String RESTAURANT_DRINKS = "RESTAURANT_DRINKS";

    private ArrayList<ItemSummary> drinks;

    public static RestaurantDrinksFragment newInstance(ArrayList<ItemSummary> drinks) {
        RestaurantDrinksFragment fragment = new RestaurantDrinksFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(RESTAURANT_DRINKS, drinks);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        drinks = getArguments().getParcelableArrayList(RESTAURANT_DRINKS);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.reusable_recycler_list, container, false);
        RestaurantItemAdapter itemAdapter = new RestaurantItemAdapter(fragmentInteractionListener,
                ItemSummary.ItemType.DRINK, drinks, this);
        RecyclerView mainContent = rootView.findViewById(R.id.recycler_list);
        mainContent.setAdapter(itemAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mainContent.setLayoutManager(layoutManager);

        return rootView;
    }

    @Override
    public void onAcceptItemAdapterInteraction(ItemSummary item) {
        AcceptItemTask acceptItemTask = new AcceptItemTask();
        acceptItemTask.execute(item.getId());
        drinks.get(drinks.indexOf(item)).setAccepted(true);
    }

    public interface RestaurantDrinksFragmentInteractionListener extends FragmentInteractionListener {
        void onRestaurantDrinksFragmentInteraction(ItemSummary foodSummary);
    }

    private class AcceptItemTask extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            RequestBody requestBody = RequestBody.create(null, new byte[]{});

            //Builds the request
            Request request = new Request.Builder()
                    .patch(requestBody)
                    .url(acceptDrinkUrl + params[0] + "/")
                    .build();

            try {
                //Makes request & handles response
                BaseApplication.getInstance().getClient().newCall(request).execute();
                return 0;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
        }
    }
}

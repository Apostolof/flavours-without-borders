package gr.auth.databases.flavours.activities.restaurant.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.annotation.NonNull;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseFragment;
import gr.auth.databases.flavours.model.DietRatingPair;
import gr.auth.databases.flavours.model.RestaurantUserView;

public class RestaurantInfoFragment extends BaseFragment implements OnMapReadyCallback {
    private MapView gMapView = null;

    public RestaurantInfoFragment() {
        // Required empty public constructor
    }

    private static final String RESTAURANT_INFO = "RESTAURANT_INFO";

    private RestaurantUserView restaurant;

    public static RestaurantInfoFragment newInstance(RestaurantUserView restaurant) {
        RestaurantInfoFragment fragment = new RestaurantInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(RESTAURANT_INFO, restaurant);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        restaurant = getArguments().getParcelable(RESTAURANT_INFO);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_restaurant_info, container, false);

        TextView workingHours = rootView.findViewById(R.id.restaurant_working_hours);
        if (restaurant.getOpeningTime() != null && restaurant.getClosingTime() != null) {
            workingHours.setText(getString(R.string.restaurant_working_hours,
                    restaurant.getOpeningTime(), restaurant.getClosingTime()));
        } else {
            workingHours.setText(getString(R.string.restaurant_working_hours, "-", "-"));
        }
        TextView type = rootView.findViewById(R.id.restaurant_type);
        type.setText(getString(R.string.restaurant_type, restaurant.getType()));
        TextView averageRatings = rootView.findViewById(R.id.restaurant_average_rating);
        if (restaurant.getAverageRating() != -1) {
            averageRatings.setText(getString(R.string.restaurant_average_rating, restaurant.getAverageRating()));
        } else {
            averageRatings.setVisibility(View.GONE);
        }

        if (!restaurant.getAverageRatingPerDiet().isEmpty()) {
            LinearLayout averageRatingPerDiet = rootView.findViewById(R.id.restaurant_average_rating_per_diet);
            averageRatingPerDiet.setVisibility(View.VISIBLE);

            for (DietRatingPair rating : restaurant.getAverageRatingPerDiet()) {
                TextView dietRatingView = new TextView(getContext());
                dietRatingView.setText(getString(R.string.restaurant_diet_average_rating,
                        rating.getDietName(), rating.getRating()));
                averageRatingPerDiet.addView(dietRatingView);
            }
        }

        gMapView = rootView.findViewById(R.id.restaurant_map);
        gMapView.getMapAsync(this);
        gMapView.onCreate(getArguments());
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (gMapView != null) {
            gMapView.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (gMapView != null) {
            gMapView.onDestroy();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (gMapView != null) {
            gMapView.onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (gMapView != null) {
            gMapView.onStop();
        }

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (gMapView != null) {
            gMapView.onSaveInstanceState(outState);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng restaurantLocation = new LatLng(restaurant.getLatitude(), restaurant.getLongitude());
        googleMap.addMarker(new MarkerOptions()
                .position(restaurantLocation)
                .title(restaurant.getName())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pub_marker)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(restaurantLocation, 12.0f));
    }
}

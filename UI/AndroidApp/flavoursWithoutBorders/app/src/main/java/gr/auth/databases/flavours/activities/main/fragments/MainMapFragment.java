package gr.auth.databases.flavours.activities.main.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.Restaurant;

public class MainMapFragment extends Fragment implements OnMapReadyCallback {
    private MapView gMapView = null;
    private static final String RESTAURANTS_TAG = "RESTAURANTS_TAG";

    private ArrayList<Restaurant> restaurants;

    public static MainMapFragment newInstance(ArrayList<Restaurant> restaurants) {
        MainMapFragment fragment = new MainMapFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(RESTAURANTS_TAG, restaurants);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        this.restaurants = getArguments().getParcelableArrayList(RESTAURANTS_TAG);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_main_map, container, false);

        gMapView = rootView.findViewById(R.id.main_map);
        gMapView.getMapAsync(this);
        gMapView.onCreate(getArguments());

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (gMapView != null) {
            gMapView.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (gMapView != null) {
            gMapView.onDestroy();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (gMapView != null) {
            gMapView.onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (gMapView != null) {
            gMapView.onStop();
        }

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (gMapView != null) {
            gMapView.onSaveInstanceState(outState);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add markers and move the camera
        for (Restaurant restaurant : restaurants) {
            LatLng restaurantPosition = new LatLng(restaurant.getLatitude(), restaurant.getLongitude());

            switch (restaurant.getType()) {
                case "restaurant":
                    googleMap.addMarker(new MarkerOptions()
                            .position(restaurantPosition)
                            .title(restaurant.getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant_marker)));
                    break;
                case "bar":
                    googleMap.addMarker(new MarkerOptions()
                            .position(restaurantPosition)
                            .title(restaurant.getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.bar_marker)));
                    break;
                case "cafeteria":
                    googleMap.addMarker(new MarkerOptions()
                            .position(restaurantPosition)
                            .title(restaurant.getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.cafeteria_marker)));
                    break;
                case "pub":
                    googleMap.addMarker(new MarkerOptions()
                            .position(restaurantPosition)
                            .title(restaurant.getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.pub_marker)));
                    break;
                case "fast food":
                    googleMap.addMarker(new MarkerOptions()
                            .position(restaurantPosition)
                            .title(restaurant.getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.fast_food_marker)));
                    break;
                case "ethnic":
                    googleMap.addMarker(new MarkerOptions()
                            .position(restaurantPosition)
                            .title(restaurant.getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.pub_marker)));
                    break;
            }
        }

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(40.6215, 22.961), 12.0f));
    }
}

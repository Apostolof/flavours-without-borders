package gr.auth.databases.flavours.activities.diets;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.Diet;
import gr.auth.databases.flavours.session.SessionManager;

import static gr.auth.databases.flavours.base.BaseActivity.getSessionManager;

public class DietsAdapter extends RecyclerView.Adapter<DietsAdapter.DietViewHolder> {
    private ArrayList<Diet> diets;
    private AcceptDietAdapterInteractionListener acceptDietAdapterInteractionListener;
    private SubscribeDietsAdapterInteractionListener subscribeDietsAdapterInteractionListener;

    DietsAdapter(ArrayList<Diet> diets,
                 AcceptDietAdapterInteractionListener acceptDietAdapterInteractionListener,
                 SubscribeDietsAdapterInteractionListener subscribeDietsAdapterInteractionListener) {
        this.diets = diets;
        this.acceptDietAdapterInteractionListener = acceptDietAdapterInteractionListener;
        this.subscribeDietsAdapterInteractionListener = subscribeDietsAdapterInteractionListener;
    }

    @NonNull
    @Override
    public DietsAdapter.DietViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                          int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.diets_row, parent, false);
        return new DietViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final DietViewHolder holder, final int position) {
        holder.name.setText(diets.get(position).getName());
        holder.description.setText(diets.get(position).getDescription());

        if (diets.get(position).isFollowedByUser()) {
            holder.addDietButton.setImageResource(R.drawable.ic_delete_black_18dp);
        } else {
            holder.addDietButton.setImageResource(R.drawable.ic_add_black_18dp);
        }

        if (!diets.get(position).isAccepted() &&
                (getSessionManager().getUserType() == SessionManager.UserType.MODERATOR)) {
            holder.acceptDietBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (acceptDietAdapterInteractionListener != null) {
                        acceptDietAdapterInteractionListener.
                                onAcceptDietAdapterInteraction(diets.get(holder.getAdapterPosition()));
                        diets.get(holder.getAdapterPosition()).setAccepted(true);

                        holder.acceptDietBtn.setVisibility(View.GONE);
                        holder.addDietButton.setVisibility(View.VISIBLE);

                        holder.addDietButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                subscribeDietsAdapterInteractionListener.onSubscribeDietsAdapterInteraction(
                                        diets.get(holder.getAdapterPosition()));

                                if ((diets.get(position).isFollowedByUser())) {
                                    holder.addDietButton.setImageResource(R.drawable.ic_add_black_18dp);
                                    diets.get(position).setFollowedByUser(!diets.get(position).isFollowedByUser());
                                } else {
                                    holder.addDietButton.setImageResource(R.drawable.ic_delete_black_18dp);
                                    diets.get(position).setFollowedByUser(!diets.get(position).isFollowedByUser());
                                }
                            }
                        });
                    }
                }
            });
            holder.acceptDietBtn.setVisibility(View.VISIBLE);
            holder.addDietButton.setVisibility(View.GONE);
        } else {
            holder.acceptDietBtn.setVisibility(View.GONE);
            holder.addDietButton.setVisibility(View.VISIBLE);

            holder.addDietButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subscribeDietsAdapterInteractionListener.onSubscribeDietsAdapterInteraction(
                            diets.get(holder.getAdapterPosition()));

                    if ((diets.get(position).isFollowedByUser())) {
                        holder.addDietButton.setImageResource(R.drawable.ic_add_black_18dp);
                        diets.get(position).setFollowedByUser(!diets.get(position).isFollowedByUser());
                    } else {
                        holder.addDietButton.setImageResource(R.drawable.ic_delete_black_18dp);
                        diets.get(position).setFollowedByUser(!diets.get(position).isFollowedByUser());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return diets.size();
    }

    static class DietViewHolder extends RecyclerView.ViewHolder {
        TextView name, description;
        AppCompatImageButton acceptDietBtn, addDietButton;

        DietViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.diets_diet_name);
            description = v.findViewById(R.id.diets_diet_description);
            acceptDietBtn = v.findViewById(R.id.restaurant_item_row_accept_btn);
            addDietButton = v.findViewById(R.id.diets_diet_subscribe);
        }
    }

    public interface AcceptDietAdapterInteractionListener {
        void onAcceptDietAdapterInteraction(Diet diet);
    }

    public interface SubscribeDietsAdapterInteractionListener {
        void onSubscribeDietsAdapterInteraction(Diet diet);
    }
}
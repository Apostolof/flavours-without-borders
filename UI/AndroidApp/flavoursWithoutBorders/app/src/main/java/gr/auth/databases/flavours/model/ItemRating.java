package gr.auth.databases.flavours.model;

import androidx.annotation.Nullable;

public class ItemRating extends Rating {
    public enum PortionSize {
        SMALL(0), MEDIUM(1), BIG(2), UNSUPPORTED(-1);

        private int id;

        PortionSize(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        @Nullable
        public static String getWorkingTypeFromId(int id) {
            switch (id) {
                case 0:
                    return "Small";
                case 1:
                    return "Medium";
                case 2:
                    return "Big";
                default:
                    return null;
            }
        }

        public static PortionSize getEnumTypeFromId(int id) {
            switch (id) {
                case 0:
                    return SMALL;
                case 1:
                    return MEDIUM;
                case 2:
                    return BIG;
                default:
                    return UNSUPPORTED;
            }
        }

        public static PortionSize getEnumTypeFromString(String possible) {
            switch (possible.toLowerCase()) {
                case "small":
                    return SMALL;
                case "medium":
                    return MEDIUM;
                case "big":
                    return BIG;
                default:
                    return UNSUPPORTED;
            }
        }
    }

    private PortionSize portionSize;

    public ItemRating(int grade, int userID, String username, String text, String date, PortionSize portionSize) {
        super(grade, userID, username, text, date);
        this.portionSize = portionSize;
    }

    public PortionSize getPortionSize() {
        return portionSize;
    }
}

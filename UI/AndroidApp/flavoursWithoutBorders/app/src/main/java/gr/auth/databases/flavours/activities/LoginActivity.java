package gr.auth.databases.flavours.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.main.MainActivity;
import gr.auth.databases.flavours.base.BaseActivity;

public class LoginActivity extends BaseActivity {
    private AppCompatButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (!sessionManager.isLoginScreenDefault()) {
            //Go to main
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
            return; //Avoid executing the code below
        }

        final EditText usernameInput = findViewById(R.id.login_username);
        final EditText passwordInput = findViewById(R.id.login_password);

        findViewById(R.id.login_btn_sign_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });

        loginButton = findViewById(R.id.login_btn_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginTask loginTask = new LoginTask();
                loginTask.execute(usernameInput.getText().toString(), passwordInput.getText().toString());
            }
        });
    }

    private class LoginTask extends AsyncTask<String, Void, Integer> {
        @Override
        protected void onPreExecute() {
            loginButton.setEnabled(false);

            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }

        @Override
        protected Integer doInBackground(String... params) {
            return sessionManager.login(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(Integer result) {
            Toast.makeText(getApplicationContext(),
                    "Welcome, " + sessionManager.getUsername() + "!", Toast.LENGTH_LONG)
                    .show();

            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            loginButton.setEnabled(true); //Re-enables login button
        }

    }
}

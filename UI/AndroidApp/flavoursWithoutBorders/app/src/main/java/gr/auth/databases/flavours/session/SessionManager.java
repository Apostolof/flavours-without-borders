package gr.auth.databases.flavours.session;

import android.content.SharedPreferences;

import com.franmontiel.persistentcookiejar.PersistentCookieJar;

import org.json.JSONObject;

import java.util.List;

import okhttp3.Cookie;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static gr.auth.databases.flavours.base.BaseApplication.CSRF_TOKEN;

public class SessionManager {
    public enum UserType {
        USER(0), MODERATOR(1), OWNER(2);

        private int id;

        UserType(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static UserType fromInteger(int x) {
            switch (x) {
                case 0:
                    return USER;
                case 1:
                    return MODERATOR;
                case 2:
                    return OWNER;
            }
            return null;
        }

    }

    //Generic constants
    private static String baseServerUrl = "http://0.0.0.0:8181/";
    public static String restaurantsUrl = baseServerUrl + "api/restaurant/";
    public static String restaurantsUserViewUrl = baseServerUrl + "api/restaurantUserView/";
    public static String getUserDietsUrl = baseServerUrl + "api/userDiets/";
    public static String rateDrinkUrl = baseServerUrl + "api/userratesdrink/";
    public static String rateFoodUrl = baseServerUrl + "api/userratesfood/";
    public static String rateRestaurantUrl = baseServerUrl + "api/userratesrestaurant/";
    public static String foodsUserViewUrl = baseServerUrl + "api/foodUserView/";
    public static String drinksUserViewUrl = baseServerUrl + "api/drinkUserView/";
    public static String profileUserViewUrl = baseServerUrl + "api/profileUserView/";
    public static String addFoodUrl = baseServerUrl + "api/addFood/";
    public static String addDrinkUrl = baseServerUrl + "api/addDrink/";
    public static String dietsUserViewUrl = baseServerUrl + "api/dietUserView/";
    public static String ingredientsUserViewUrl = baseServerUrl + "api/ingredientUserView/";
    public static String prohibitIngredientUrl = baseServerUrl + "api/prohibitIngredient/";
    public static String followDietUrl = baseServerUrl + "api/followDiet/";
    public static String addIngredientUrl = baseServerUrl + "api/ingredient/";
    public static String addDietUrl = baseServerUrl + "api/addDiet/";
    public static String addIngredientToFoodUrl = baseServerUrl + "api/foodhasingredient/";
    public static String addIngredientToDrinkUrl = baseServerUrl + "api/drinkhasingredient/";
    public static String acceptRestaurantUrl = baseServerUrl + "api/acceptRestaurant/";
    public static String acceptFoodUrl = baseServerUrl + "api/acceptFood/";
    public static String acceptDrinkUrl = baseServerUrl + "api/acceptDrink/";
    public static String acceptDietUrl = baseServerUrl + "api/acceptDiet/";
    private static String loginUrl = baseServerUrl + "api/rest-auth/login/";
    private static String signupUrl = baseServerUrl + "api/rest-auth/registration/";
    private static String logoutUrl = baseServerUrl + "api/rest-auth/logout/";
    private static String setUserBirthday = baseServerUrl + "api/setUserBirthday/";

    // Client & Cookies
    private final OkHttpClient client;
    private final PersistentCookieJar cookieJar;

    //Shared Preferences & its keys
    private final SharedPreferences sharedPrefs;
    private static final String USERNAME = "Username";
    private static final String USER_ID = "UserID";
    private static final String USER_TYPE = "USER_TYPE";
    private static final String LOGGED_IN = "LoggedIn";
    private static final String LOGIN_SCREEN_AS_DEFAULT = "LoginScreenAsDefault";

    //Constructor
    public SessionManager(OkHttpClient client, PersistentCookieJar cookieJar, SharedPreferences sharedPrefs, String baseServerIP) {
        this.client = client;
        this.cookieJar = cookieJar;
        this.sharedPrefs = sharedPrefs;

        if (baseServerIP != null && !baseServerIP.isEmpty()) {
            setBaseServerUrl(baseServerIP);
        }
    }

    public static void setBaseServerUrl(String baseServerIP) {
        // Sets base url
        SessionManager.baseServerUrl = "http://" + baseServerIP + ":8181/";

        // Sets all other api point urls
        loginUrl = baseServerUrl + "api/rest-auth/login/";
        signupUrl = baseServerUrl + "api/rest-auth/registration/";
        logoutUrl = baseServerUrl + "api/rest-auth/logout/";
        setUserBirthday = baseServerUrl + "api/setUserBirthday/";
        restaurantsUrl = baseServerUrl + "api/restaurant/";
        restaurantsUserViewUrl = baseServerUrl + "api/restaurantUserView/";
        getUserDietsUrl = baseServerUrl + "api/userDiets/";
        rateDrinkUrl = baseServerUrl + "api/userratesdrink/";
        rateFoodUrl = baseServerUrl + "api/userratesfood/";
        rateRestaurantUrl = baseServerUrl + "api/userratesrestaurant/";
        foodsUserViewUrl = baseServerUrl + "api/foodUserView/";
        drinksUserViewUrl = baseServerUrl + "api/drinkUserView/";
        profileUserViewUrl = baseServerUrl + "api/profileUserView/";
        addFoodUrl = baseServerUrl + "api/addFood/";
        addDrinkUrl = baseServerUrl + "api/addDrink/";
        dietsUserViewUrl = baseServerUrl + "api/dietUserView/";
        ingredientsUserViewUrl = baseServerUrl + "api/ingredientUserView/";
        prohibitIngredientUrl = baseServerUrl + "api/prohibitIngredient/";
        followDietUrl = baseServerUrl + "api/followDiet/";
        addIngredientUrl = baseServerUrl + "api/ingredient/";
        addDietUrl = baseServerUrl + "api/addDiet/";
        addIngredientToFoodUrl = baseServerUrl + "api/foodhasingredient/";
        addIngredientToDrinkUrl = baseServerUrl + "api/drinkhasingredient/";
        acceptRestaurantUrl = baseServerUrl + "api/acceptRestaurant/";
        acceptFoodUrl = baseServerUrl + "api/acceptFood/";
        acceptDrinkUrl = baseServerUrl + "api/acceptDrink/";
        acceptDietUrl = baseServerUrl + "api/acceptDiet/";
    }

    public int signup(String... strings) {
        //Builds the signup request
        Request request;

        clearSessionData();

        String email = strings[0];
        String username = strings[1];
        String password = strings[2];
        String birthday = strings[3];

        RequestBody formBody = new FormBody.Builder()
                .add("email", email)
                .add("username", username)
                .add("password1", password)
                .add("password2", password)
                .build();
        request = new Request.Builder()
                .url(signupUrl)
                .post(formBody)
                .build();

        try {
            //Makes request & handles response
            Response response = client.newCall(request).execute();

            setPersistentCookieSession(signupUrl);   //Store cookies

            ResponseBody responseBody = response.body();
            assert responseBody != null;
            JSONObject jsonResponse = new JSONObject(responseBody.string());
            JSONObject jsonUser = jsonResponse.getJSONObject("user");
            int extractedUserId = jsonUser.getInt("id");
            int extractedRole = jsonUser.getInt("role");

            //Edit SharedPreferences, save session's data
            SharedPreferences.Editor editor = sharedPrefs.edit();
            setLoginScreenAsDefault(false);
            editor.putBoolean(LOGGED_IN, true);
            editor.putString(USERNAME, username);
            editor.putInt(USER_ID, extractedUserId);
            editor.putInt(USER_TYPE, extractedRole);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
            return 2;
        }

        formBody = new FormBody.Builder()
                .add("user_age", birthday)
                .build();
        request = new Request.Builder()
                .url(setUserBirthday + getUserId() + "/")
                .put(formBody)
                .build();

        try {
            //Makes request & handles response
            client.newCall(request).execute();
            return 0;
        } catch (Exception e) {
            return 2;
        }
    }

    public int logout() {
        RequestBody requestBody = RequestBody.create(null, new byte[]{});

        Request request = new Request.Builder()
                .url(logoutUrl)
                .method("POST", requestBody)
                .header("Content-Length", "0")
                .build();

        try {
            //Makes request & handles response
            client.newCall(request).execute();
        } catch (Exception e) {
            return 2;
        } finally {
            //All data should always be cleared from device regardless the result of logout
            clearSessionData();
        }

        return 0;
    }

    public String getUsername() {
        return sharedPrefs.getString(USERNAME, USERNAME);
    }

    public int getUserId() {
        return sharedPrefs.getInt(USER_ID, -1);
    }

    public UserType getUserType() {
        return UserType.fromInteger(sharedPrefs.getInt(USER_TYPE, -1));
    }

    public int login(String... strings) {
        //Builds the login request for each case
        Request request;
        clearSessionData();

        String username = strings[0];
        String password = strings[1];

        RequestBody formBody = new FormBody.Builder()
                .add("username", username)
                .add("password", password)
                .build();
        request = new Request.Builder()
                .url(loginUrl)
                .post(formBody)
                .build();

        try {
            //Makes request & handles response
            Response response = client.newCall(request).execute();

            setPersistentCookieSession(loginUrl);   //Store cookies

            ResponseBody responseBody = response.body();
            assert responseBody != null;
            JSONObject jsonResponse = new JSONObject(responseBody.string());
            JSONObject jsonUser = jsonResponse.getJSONObject("user");
            int extractedUserId = jsonUser.getInt("id");
            int extractedRole = jsonUser.getInt("role");

            //Edit SharedPreferences, save session's data
            SharedPreferences.Editor editor = sharedPrefs.edit();
            setLoginScreenAsDefault(false);
            editor.putBoolean(LOGGED_IN, true);
            editor.putString(USERNAME, username);
            editor.putInt(USER_ID, extractedUserId);
            editor.putInt(USER_TYPE, extractedRole);
            editor.apply();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 2;
        }
    }

    public boolean isLoggedIn() {
        return sharedPrefs.getBoolean(LOGGED_IN, false);
    }

    public boolean isLoginScreenDefault() {
        return sharedPrefs.getBoolean(LOGIN_SCREEN_AS_DEFAULT, true);
    }

    private void setPersistentCookieSession(String forUrl) {
        HttpUrl httpUrl = HttpUrl.parse(forUrl);
        assert httpUrl != null;
        List<Cookie> cookieList = cookieJar.loadForRequest(httpUrl);
        String csrfToken = "";
        int maxLength = 0;

        SharedPreferences.Editor editor = sharedPrefs.edit();

        for (int cookieIndex = 0; cookieIndex < cookieList.size(); ++cookieIndex) {
            Cookie cookie = cookieList.get(cookieIndex);
            if (!cookie.value().contains("Successfully signed in") && cookie.value().length() > maxLength) {
                // The longest one is the CSRF token
                csrfToken = cookie.value();
                maxLength = csrfToken.length();
            }
        }

        editor.putString(CSRF_TOKEN, csrfToken);
        editor.apply();
    }

    private void clearSessionData() {
        cookieJar.clear();
        sharedPrefs.edit().clear().apply(); //Clear session data
        sharedPrefs.edit().putString(USERNAME, "Guest").apply();
        sharedPrefs.edit().putInt(USER_ID, -1).apply();
        sharedPrefs.edit().putInt(USER_TYPE, -1).apply();
        sharedPrefs.edit().putBoolean(LOGGED_IN, false).apply(); //User logs out
        setLoginScreenAsDefault(true);
    }

    private void setLoginScreenAsDefault(boolean b) {
        sharedPrefs.edit().putBoolean(LOGIN_SCREEN_AS_DEFAULT, b).apply();
    }
}
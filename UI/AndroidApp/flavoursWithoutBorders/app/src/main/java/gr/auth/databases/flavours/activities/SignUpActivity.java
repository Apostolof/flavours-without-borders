package gr.auth.databases.flavours.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.DialogFragment;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.main.MainActivity;
import gr.auth.databases.flavours.base.BaseActivity;

public class SignUpActivity extends BaseActivity {
    private EditText emailInput, usernameInput, passwordInput, birthdayInput;
    private AppCompatButton signupButton;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        findViewById(R.id.sign_up_btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        emailInput = findViewById(R.id.sign_up_email);
        usernameInput = findViewById(R.id.sign_up_username);
        passwordInput = findViewById(R.id.sign_up_password);
        birthdayInput = findViewById(R.id.sign_up_birthday);
        birthdayInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    DialogFragment newFragment = new DatePickerFragment();
                    newFragment.show(getSupportFragmentManager(), "datePicker");
                }
                return false;
            }
        });

        signupButton = findViewById(R.id.sign_up_btn_sign_up);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignupTask signupTask = new SignupTask();
                signupTask.execute(emailInput.getText().toString(),
                        usernameInput.getText().toString(),
                        passwordInput.getText().toString(),
                        birthdayInput.getText().toString());
            }
        });
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        private EditText birthdayInput;

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            birthdayInput = Objects.requireNonNull(getActivity()).findViewById(R.id.sign_up_birthday);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            birthdayInput.setText(getResources().getString(R.string.sign_up_birthday_placeholder, year,
                    ((month < 9) ? "0" : "") + (month + 1),
                    ((day < 10) ? "0" : "") + day));
        }
    }

    private class SignupTask extends AsyncTask<String, Void, Integer> {
        @Override
        protected void onPreExecute() {
            signupButton.setEnabled(false);

            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }

        @Override
        protected Integer doInBackground(String... params) {
            return sessionManager.signup(params[0], params[1], params[2], params[3]);
        }

        @Override
        protected void onPostExecute(Integer result) {
            Toast.makeText(getApplicationContext(),
                    "Welcome, " + sessionManager.getUsername() + "!", Toast.LENGTH_LONG)
                    .show();

            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            signupButton.setEnabled(true); //Re-enable login button
        }

    }
}

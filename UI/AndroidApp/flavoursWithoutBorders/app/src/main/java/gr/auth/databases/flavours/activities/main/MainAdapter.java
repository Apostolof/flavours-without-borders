package gr.auth.databases.flavours.activities.main;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.restaurant.RestaurantActivity;
import gr.auth.databases.flavours.model.Restaurant;
import gr.auth.databases.flavours.session.SessionManager;

import static gr.auth.databases.flavours.activities.restaurant.RestaurantActivity.BUNDLE_ARG_RESTAURANT;
import static gr.auth.databases.flavours.base.BaseActivity.getSessionManager;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.RestaurantViewHolder> {
    private Context context;
    private ArrayList<Restaurant> restaurants;
    private AcceptRestaurantAdapterInteractionListener acceptRestaurantAdapterInteractionListener;

    public MainAdapter(Context context, ArrayList<Restaurant> restaurants,
                       AcceptRestaurantAdapterInteractionListener acceptRestaurantAdapterInteractionListener) {
        this.context = context;
        this.restaurants = restaurants;
        this.acceptRestaurantAdapterInteractionListener = acceptRestaurantAdapterInteractionListener;
    }

    @NonNull
    @Override
    public MainAdapter.RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                               int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main_list_row,
                parent, false);
        return new RestaurantViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RestaurantViewHolder holder, int position) {
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RestaurantActivity.class);
                intent.putExtra(BUNDLE_ARG_RESTAURANT, restaurants.get(holder.getAdapterPosition()));
                context.startActivity(intent);
            }
        });
        holder.name.setText(restaurants.get(position).getName());
        if (!restaurants.get(position).getOpeningTime().equals("null") &&
                !restaurants.get(position).getClosingTime().equals("null")) {
            holder.workingHours.setVisibility(View.VISIBLE);
            holder.workingHours.setText(context.getString(R.string.main_row_working_hours_placeholder,
                    restaurants.get(position).getOpeningTime().substring(0, 5),
                    restaurants.get(position).getClosingTime().substring(0, 5)));
        } else {
            holder.workingHours.setVisibility(View.GONE);
        }

        switch (restaurants.get(position).getType()) {
            case "restaurant":
                holder.typeImage.setImageResource(R.drawable.restaurant_marker);
                holder.typeText.setText(context.getResources().getString(R.string.main_restaurant_type));
                break;
            case "bar":
                holder.typeImage.setImageResource(R.drawable.bar_marker);
                holder.typeText.setText(context.getResources().getString(R.string.main_bar_type));
                break;
            case "cafeteria":
                holder.typeImage.setImageResource(R.drawable.cafeteria_marker);
                holder.typeText.setText(context.getResources().getString(R.string.main_cafeteria_type));
                break;
            case "pub":
                holder.typeImage.setImageResource(R.drawable.pub_marker);
                holder.typeText.setText(context.getResources().getString(R.string.main_pub_type));
                break;
            case "fast food":
                holder.typeImage.setImageResource(R.drawable.fast_food_marker);
                holder.typeText.setText(context.getResources().getString(R.string.main_fast_food_type));
                break;
            case "ethnic":
                holder.typeImage.setImageResource(R.drawable.restaurant_marker);
                holder.typeText.setText(context.getResources().getString(R.string.main_ethnic_type));
                break;
        }

        if (!restaurants.get(position).isAccepted() &&
                (getSessionManager().getUserType() == SessionManager.UserType.MODERATOR)) {
            holder.acceptRestaurantBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (acceptRestaurantAdapterInteractionListener != null) {
                        acceptRestaurantAdapterInteractionListener.
                                onAcceptRestaurantAdapterInteraction(restaurants.get(holder.getAdapterPosition()));
                        restaurants.get(holder.getAdapterPosition()).setAccepted(true);
                        holder.acceptRestaurantBtn.setVisibility(View.GONE);
                    }
                }
            });
            holder.acceptRestaurantBtn.setVisibility(View.VISIBLE);
        } else {
            holder.acceptRestaurantBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return restaurants.size();
    }

    static class RestaurantViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        TextView name, typeText, workingHours;
        ImageView typeImage;
        AppCompatImageButton acceptRestaurantBtn;

        RestaurantViewHolder(View v) {
            super(v);
            card = v.findViewById(R.id.main_row_card);
            name = v.findViewById(R.id.main_row_name);
            typeText = v.findViewById(R.id.main_row_type_txt);
            workingHours = v.findViewById(R.id.main_row_working_hours);
            typeImage = v.findViewById(R.id.main_row_type_img);
            acceptRestaurantBtn = v.findViewById(R.id.main_row_accept_restaurant_btn);
        }
    }

    public interface AcceptRestaurantAdapterInteractionListener {
        void onAcceptRestaurantAdapterInteraction(Restaurant restaurant);
    }
}
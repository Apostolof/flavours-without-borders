package gr.auth.databases.flavours.activities.restaurant.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseFragment;
import gr.auth.databases.flavours.model.ItemSummary;
import gr.auth.databases.flavours.session.SessionManager;

import static gr.auth.databases.flavours.base.BaseActivity.getSessionManager;

public class RestaurantItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private RestaurantFoodsFragment.RestaurantFoodsFragmentInteractionListener foodInteractionListener;
    private RestaurantDrinksFragment.RestaurantDrinksFragmentInteractionListener drinkInteractionListener;
    private final ArrayList<ItemSummary> items;
    private AcceptItemAdapterInteractionListener acceptItemAdapterInteractionListener;

    RestaurantItemAdapter(BaseFragment.FragmentInteractionListener interactionListener,
                          ItemSummary.ItemType itemType, ArrayList<ItemSummary> items,
                          AcceptItemAdapterInteractionListener acceptRestaurantAdapterInteractionListener) {
        if (itemType == ItemSummary.ItemType.FOOD) {
            this.foodInteractionListener = (RestaurantFoodsFragment.RestaurantFoodsFragmentInteractionListener) interactionListener;
        } else if (itemType == ItemSummary.ItemType.DRINK) {
            this.drinkInteractionListener = (RestaurantDrinksFragment.RestaurantDrinksFragmentInteractionListener) interactionListener;
        }

        this.items = items;
        this.acceptItemAdapterInteractionListener = acceptRestaurantAdapterInteractionListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.restaurant_item_row, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        ItemSummary item = items.get(position);
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (items.get(holder.getAdapterPosition()).getType() == ItemSummary.ItemType.FOOD &&
                        foodInteractionListener != null) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    foodInteractionListener.onRestaurantFoodsFragmentInteraction(items.get(holder.getAdapterPosition()));
                } else if (items.get(holder.getAdapterPosition()).getType() == ItemSummary.ItemType.DRINK
                        && drinkInteractionListener != null) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    drinkInteractionListener.onRestaurantDrinksFragmentInteraction(items.get(holder.getAdapterPosition()));
                }
            }
        });

        itemViewHolder.item.setText(item.getItemName());

        if (!items.get(position).isAccepted() &&
                (getSessionManager().getUserType() == SessionManager.UserType.MODERATOR)) {
            itemViewHolder.acceptItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (acceptItemAdapterInteractionListener != null) {
                        acceptItemAdapterInteractionListener.
                                onAcceptItemAdapterInteraction(items.get(holder.getAdapterPosition()));
                        items.get(holder.getAdapterPosition()).setAccepted(true);
                        itemViewHolder.acceptItem.setVisibility(View.GONE);
                    }
                }
            });
            itemViewHolder.acceptItem.setVisibility(View.VISIBLE);
        } else {
            itemViewHolder.acceptItem.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        TextView item;
        AppCompatImageButton acceptItem;

        ItemViewHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.restaurant_item_row_card);
            item = itemView.findViewById(R.id.restaurant_item_row_item);
            acceptItem = itemView.findViewById(R.id.restaurant_item_row_accept_btn);
        }
    }

    public interface AcceptItemAdapterInteractionListener {
        void onAcceptItemAdapterInteraction(ItemSummary item);
    }
}

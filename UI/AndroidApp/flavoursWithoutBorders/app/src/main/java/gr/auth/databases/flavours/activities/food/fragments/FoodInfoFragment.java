package gr.auth.databases.flavours.activities.food.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.Food;

public class FoodInfoFragment extends Fragment {

    public FoodInfoFragment() {
        // Required empty public constructor
    }

    private static final String FOOD_INFO = "FOOD_INFO";

    private Food food;

    public static FoodInfoFragment newInstance(Food food) {
        FoodInfoFragment fragment = new FoodInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(FOOD_INFO, food);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        food = getArguments().getParcelable(FOOD_INFO);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_food_info, container, false);

        TextView restaurantServing = rootView.findViewById(R.id.food_serving_restaurant);
        restaurantServing.setText(getString(R.string.food_serving_restaurant_placeholder, food.getRestaurantName()));
        TextView calories = rootView.findViewById(R.id.food_calories);
        if (food.getCalories() != -1) {
            calories.setText(getString(R.string.food_calories_placeholder, food.getCalories()));
        } else {
            calories.setVisibility(View.GONE);
        }
        TextView rating = rootView.findViewById(R.id.food_rating);
        if (food.getRating() != -1) {
            rating.setText(getString(R.string.food_rating_placeholder, food.getRating()));
        } else {
            rating.setVisibility(View.GONE);
        }
        TextView description = rootView.findViewById(R.id.food_description);
        if (!food.getDescription().equals("null")) {
            description.setText(food.getDescription());
        } else {
            description.setVisibility(View.GONE);
        }

        return rootView;
    }
}

package gr.auth.databases.flavours.activities.diets;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.AddDietActivity;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.base.BaseApplication;
import gr.auth.databases.flavours.model.Diet;
import gr.auth.databases.flavours.utils.FollowDietStateTask;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static gr.auth.databases.flavours.session.SessionManager.acceptDietUrl;
import static gr.auth.databases.flavours.session.SessionManager.dietsUserViewUrl;

public class DietsActivity extends BaseActivity implements
        DietsAdapter.AcceptDietAdapterInteractionListener, DietsAdapter.SubscribeDietsAdapterInteractionListener {
    private RecyclerView recyclerView;
    private FloatingActionButton FAB;
    private ArrayList<Diet> diets = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diets);

        Toolbar toolbar = findViewById(R.id.diets_toolbar);
        toolbar.setTitle(getString(R.string.diets_toolbar_title));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        createDrawer();

        FAB = findViewById(R.id.diets_fab);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), AddDietActivity.class);
                startActivity(intent);
            }
        });

        recyclerView = findViewById(R.id.diets_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        DietsTask dietsTask = new DietsTask();
        dietsTask.execute();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAcceptDietAdapterInteraction(Diet diet) {
        AcceptDietTask acceptDietTask = new AcceptDietTask();
        acceptDietTask.execute(diet.getId());
        diets.get(diets.indexOf(diet)).setAccepted(true);
    }

    @Override
    public void onSubscribeDietsAdapterInteraction(Diet diet) {
        FollowDietStateTask followDietStateTask = new FollowDietStateTask(client);
        followDietStateTask.execute(diet.getId());
    }

    private class DietsTask extends AsyncTask<Void, Void, Integer> {
        private static final String JSON_TAG_DIETS_LIST = "diets";
        private static final String JSON_TAG_DIET_ID = "diet_id";
        private static final String JSON_TAG_DIET_NAME = "diet_name";
        private static final String JSON_TAG_DIET_DESCRIPTION = "diet_description";
        private static final String JSON_TAG_DIET_IS_ACCEPTED = "diet_is_approved";

        private static final String JSON_TAG_USER_FOLLOWS_DIET_LIST = "followed";
        private static final String JSON_TAG_USER_FOLLOWS_DIET_DIET = "diet";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            //Builds the request
            Request request = new Request.Builder()
                    .url(dietsUserViewUrl)
                    .build();

            try {
                //Makes request & handles response
                Response response = client.newCall(request).execute();

                ResponseBody responseBody = response.body();
                assert responseBody != null;
                String result = responseBody.string();
                JSONObject jsonResponse = new JSONObject(result);

                ArrayList<Integer> userDiets = new ArrayList<>();
                JSONArray jsonUserDiets = jsonResponse.getJSONArray(JSON_TAG_USER_FOLLOWS_DIET_LIST);
                for (int dietIndex = 0; dietIndex < jsonUserDiets.length(); ++dietIndex) {
                    JSONObject diet = jsonUserDiets.getJSONObject(dietIndex);
                    userDiets.add(diet.getInt(JSON_TAG_USER_FOLLOWS_DIET_DIET));
                }

                JSONArray jsonDietsList = jsonResponse.getJSONArray(JSON_TAG_DIETS_LIST);
                for (int dietIndex = 0; dietIndex < jsonDietsList.length(); ++dietIndex) {
                    JSONObject diet = jsonDietsList.getJSONObject(dietIndex);
                    diets.add(new Diet(diet.getInt(JSON_TAG_DIET_ID),
                            diet.getString(JSON_TAG_DIET_NAME),
                            diet.getString(JSON_TAG_DIET_DESCRIPTION),
                            userDiets.contains(diet.getInt(JSON_TAG_DIET_ID)),
                            diet.getBoolean(JSON_TAG_DIET_IS_ACCEPTED)));
                }
                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            DietsAdapter dietsAdapter = new DietsAdapter(diets, DietsActivity.this,
                    DietsActivity.this);
            recyclerView.setAdapter(dietsAdapter);
        }
    }

    private class AcceptDietTask extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            RequestBody requestBody = RequestBody.create(null, new byte[]{});

            //Builds the request
            Request request = new Request.Builder()
                    .patch(requestBody)
                    .url(acceptDietUrl + params[0] + "/")
                    .build();

            try {
                //Makes request & handles response
                BaseApplication.getInstance().getClient().newCall(request).execute();
                return 0;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
        }
    }
}

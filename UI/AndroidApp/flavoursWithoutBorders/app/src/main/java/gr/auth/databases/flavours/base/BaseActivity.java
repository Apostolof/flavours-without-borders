package gr.auth.databases.flavours.base;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.LoginActivity;
import gr.auth.databases.flavours.activities.diets.DietsActivity;
import gr.auth.databases.flavours.activities.ingredients.IngredientsActivity;
import gr.auth.databases.flavours.activities.main.MainActivity;
import gr.auth.databases.flavours.activities.profile.ProfileActivity;
import gr.auth.databases.flavours.session.SessionManager;
import okhttp3.OkHttpClient;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static gr.auth.databases.flavours.activities.profile.ProfileActivity.BUNDLE_USER_ID;
import static gr.auth.databases.flavours.activities.profile.ProfileActivity.BUNDLE_USER_NAME;

public abstract class BaseActivity extends AppCompatActivity {
    public static final String PREF_BASE_SERVER_IP = "PREF_BASE_SERVER_IP";
    // Client & Cookies
    protected static OkHttpClient client;

    //SessionManager
    protected static SessionManager sessionManager;

    private SharedPreferences sharedPreferences;

    //Common UI elements
    protected Toolbar toolbar;
    protected Drawer drawer;

    private boolean isBaseIPSetDialogShown = false;
    private boolean isMainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isMainActivity = this instanceof MainActivity;

        if (client == null)
            client = BaseApplication.getInstance().getClient(); //must check every time

        // they become null when app restarts after crash
        if (sessionManager == null)
            sessionManager = BaseApplication.getInstance().getSessionManager();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (!checkPerms()) {
            requestPerms();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateDrawer();
        String baseServerIP = sharedPreferences.getString(PREF_BASE_SERVER_IP, null);
        if ((baseServerIP == null || baseServerIP.isEmpty()) && !isBaseIPSetDialogShown) {
            isBaseIPSetDialogShown = true;
            showBaseIPSetDialog();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (drawer != null)    //close drawer animation after returning to activity
            drawer.closeDrawer();
    }

    public static OkHttpClient getClient() {
        return client;
    }

    public static SessionManager getSessionManager() {
        return sessionManager;
    }

    //------------------------------------------DRAWER STUFF----------------------------------------
    protected static final int HOME_ID = 0;
    protected static final int DIETS_ID = 1;
    protected static final int INGREDIENTS_ID = 2;
    protected static final int LOG_ID = 3;

    private AccountHeader accountHeader;
    private ProfileDrawerItem profileDrawerItem;
    private PrimaryDrawerItem loginLogoutItem;

    protected void createDrawer() {
        final int primaryColor = ContextCompat.getColor(this, R.color.textPrimary);
        final int selectedPrimaryColor = ContextCompat.getColor(this, R.color.primary_dark);
        final int selectedSecondaryColor = ContextCompat.getColor(this, R.color.iron);

        PrimaryDrawerItem homeItem, dietsItem, ingredientsItem;
        IconicsDrawable homeIcon, homeIconSelected, dietsIcon, dietsIconSelected, ingredientsIcon, ingredientsIconSelected;

        //Drawer Icons
        homeIcon = new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_home)
                .color(primaryColor);

        homeIconSelected = new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_home)
                .color(selectedSecondaryColor);

        dietsIcon = new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_donut_small)
                .color(primaryColor);

        dietsIconSelected = new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_donut_small)
                .color(selectedSecondaryColor);

        ingredientsIcon = new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_kitchen)
                .color(primaryColor);

        ingredientsIconSelected = new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_kitchen)
                .color(selectedSecondaryColor);

        IconicsDrawable loginIcon = new IconicsDrawable(this)
                .icon(FontAwesome.Icon.faw_sign_in)
                .color(primaryColor);

        IconicsDrawable logoutIcon = new IconicsDrawable(this)
                .icon(FontAwesome.Icon.faw_sign_out)
                .color(primaryColor);

        //Drawer Items
        homeItem = new PrimaryDrawerItem()
                .withTextColor(primaryColor)
                .withSelectedColor(selectedPrimaryColor)
                .withSelectedTextColor(selectedSecondaryColor)
                .withIdentifier(HOME_ID)
                .withName(R.string.drawer_home)
                .withIcon(homeIcon)
                .withSelectedIcon(homeIconSelected);

        dietsItem = new PrimaryDrawerItem()
                .withTextColor(primaryColor)
                .withSelectedColor(selectedPrimaryColor)
                .withSelectedTextColor(selectedSecondaryColor)
                .withIdentifier(DIETS_ID)
                .withName(R.string.drawer_diets)
                .withIcon(dietsIcon)
                .withSelectedIcon(dietsIconSelected);

        ingredientsItem = new PrimaryDrawerItem()
                .withTextColor(primaryColor)
                .withSelectedColor(selectedPrimaryColor)
                .withSelectedTextColor(selectedSecondaryColor)
                .withIdentifier(INGREDIENTS_ID)
                .withName(R.string.drawer_ingredients)
                .withIcon(ingredientsIcon)
                .withSelectedIcon(ingredientsIconSelected);

        if (sessionManager.isLoggedIn()) {
            loginLogoutItem = new PrimaryDrawerItem()
                    .withTextColor(primaryColor)
                    .withSelectedColor(selectedSecondaryColor)
                    .withIdentifier(LOG_ID)
                    .withName(R.string.drawer_logout)
                    .withIcon(logoutIcon)
                    .withSelectable(false);
        } else {
            loginLogoutItem = new PrimaryDrawerItem()
                    .withTextColor(primaryColor)
                    .withSelectedColor(selectedSecondaryColor)
                    .withIdentifier(LOG_ID).withName(R.string.drawer_login)
                    .withIcon(loginIcon)
                    .withSelectable(false);
        }

        //Profile
        profileDrawerItem = new ProfileDrawerItem().withName(sessionManager.getUsername()).withIdentifier(0);

        //AccountHeader
        accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withSelectionListEnabledForSingleProfile(false)
                .withHeaderBackground(R.color.primary)
                .withTextColor(getResources().getColor(R.color.iron))
                .addProfiles(profileDrawerItem)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        if (sessionManager.isLoggedIn()) {
                            Intent intent = new Intent(BaseActivity.this, ProfileActivity.class);
                            Bundle extras = new Bundle();
                            extras.putInt(BUNDLE_USER_ID, sessionManager.getUserId());
                            extras.putString(BUNDLE_USER_NAME, sessionManager.getUsername());
                            intent.putExtras(extras);
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            return false;
                        } else
                            startLoginActivity();
                        return true;
                    }
                })
                .build();

        DrawerBuilder drawerBuilder = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withDrawerWidthDp((int) BaseApplication.getInstance().getDpWidth() / 2)
                .withSliderBackgroundColor(ContextCompat.getColor(this, R.color.colorBackground))
                .withAccountHeader(accountHeader)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem.equals(HOME_ID)) {
                            if (!isMainActivity) {
                                Intent intent = new Intent(BaseActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                        } else if (drawerItem.equals(DIETS_ID)) {
                            Intent intent = new Intent(BaseActivity.this, DietsActivity.class);
                            startActivity(intent);
                        } else if (drawerItem.equals(INGREDIENTS_ID)) {
                            Intent intent = new Intent(BaseActivity.this, IngredientsActivity.class);
                            startActivity(intent);
                        } else if (drawerItem.equals(LOG_ID)) {
                            if (!sessionManager.isLoggedIn()) //When logged out or if user is guest
                                startLoginActivity();
                            else
                                new LogoutTask().execute();
                        }

                        drawer.closeDrawer();
                        return true;
                    }
                });

        drawerBuilder.addDrawerItems(homeItem, dietsItem, ingredientsItem, loginLogoutItem);
        drawer = drawerBuilder.build();

        /*if (!isMainActivity)
            drawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);*/

        drawer.setOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
            @Override
            public boolean onNavigationClickListener(View clickedView) {
                onBackPressed();
                return true;
            }
        });
    }

    private void updateDrawer() {
        if (drawer != null) {
            accountHeader.updateProfile(profileDrawerItem);
            drawer.updateItem(loginLogoutItem);
        }
    }

    //-------PERMS---------
    private static final int PERMISSIONS_REQUEST_CODE = 69;

    //True if permissions are OK
    private boolean checkPerms() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            String[] PERMISSIONS_MAP = {
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.INTERNET};

            return !(checkSelfPermission(PERMISSIONS_MAP[0]) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(PERMISSIONS_MAP[1]) == PackageManager.PERMISSION_DENIED);
        }
        return true;
    }

    //Display popup for user to grant permission
    private void requestPerms() { //Runtime permissions request for devices with API >= 23
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            String[] PERMISSIONS_MAP = {
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.INTERNET};

            requestPermissions(PERMISSIONS_MAP, PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, @NonNull String[] permissions
            , @NonNull int[] grantResults) {
        switch (permsRequestCode) {
            case PERMISSIONS_REQUEST_CODE:
                break;
        }
    }

    private void startLoginActivity() {
        Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    private class LogoutTask extends AsyncTask<Void, Void, Integer> {
        protected Integer doInBackground(Void... voids) {
            return sessionManager.logout();
        }

        protected void onPreExecute() {
        }

        protected void onPostExecute(Integer result) {
            updateDrawer();
        }
    }

    private void showBaseIPSetDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getThemedContext());

        final EditText serverIPInput = new EditText(this);
        serverIPInput.setInputType(InputType.TYPE_CLASS_PHONE);

        builder.setTitle("Base server IP address")
                .setView(serverIPInput)
                .setMessage("Please provide the base IP of the server (xxx.xxx.xxx.xxx) to be used in the requests.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(PREF_BASE_SERVER_IP, serverIPInput.getText().toString()).apply();
                        SessionManager.setBaseServerUrl(serverIPInput.getText().toString());
                        isBaseIPSetDialogShown = false;
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}

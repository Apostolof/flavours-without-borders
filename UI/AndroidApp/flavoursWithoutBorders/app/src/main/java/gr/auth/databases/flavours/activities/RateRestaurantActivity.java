package gr.auth.databases.flavours.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.DietSummary;
import gr.auth.databases.flavours.model.Restaurant;
import gr.auth.databases.flavours.model.RestaurantRating;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static gr.auth.databases.flavours.session.SessionManager.getUserDietsUrl;
import static gr.auth.databases.flavours.session.SessionManager.rateRestaurantUrl;

public class RateRestaurantActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    public static final String BUNDLE_RATE_RESTAURANT = "BUNDLE_RATE_RESTAURANT";

    List<String> spinnerArray = new ArrayList<>();
    List<DietSummary> userDiets = new ArrayList<>();

    private Restaurant restaurant;
    private EditText ratingText;
    private int ratingGrade = -1;
    private RestaurantRating.Accessibility ratingAccessibility;
    private int ratingDiet = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_restaurant);

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        restaurant = extras.getParcelable(BUNDLE_RATE_RESTAURANT);

        Toolbar toolbar = findViewById(R.id.rate_restaurant_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        Spinner gradeSpinner = findViewById(R.id.rate_restaurant_grade);
        ArrayAdapter<CharSequence> gradeAdapter = ArrayAdapter.createFromResource(this,
                R.array.rating_grade, android.R.layout.simple_spinner_item);
        gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gradeSpinner.setAdapter(gradeAdapter);
        gradeSpinner.setOnItemSelectedListener(this);

        Spinner accessibilitySpinner = findViewById(R.id.rate_restaurant_accessibility);
        ArrayAdapter<CharSequence> accessibilityAdapter = ArrayAdapter.createFromResource(this,
                R.array.rating_accessibility, android.R.layout.simple_spinner_item);
        accessibilityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        accessibilitySpinner.setAdapter(accessibilityAdapter);
        accessibilitySpinner.setOnItemSelectedListener(this);

        ratingText = findViewById(R.id.rate_restaurant_text);

        AppCompatButton rateButton = findViewById(R.id.rate_restaurant_rate_btn);
        rateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateRestaurantTask rateRestaurantTask = new RateRestaurantTask();
                rateRestaurantTask.execute();
            }
        });

        createDrawer();
        drawer.setSelection(-1);

        GetUserDietsTask getUserDietsTask = new GetUserDietsTask();
        getUserDietsTask.execute();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        if (parent.getId() == R.id.rate_restaurant_grade) {
            ratingGrade = pos + 1;
        } else if (parent.getId() == R.id.rate_restaurant_accessibility) {
            ratingAccessibility = RestaurantRating.Accessibility.getEnumTypeFromId(pos);
        } else if (parent.getId() == R.id.rate_restaurant_diet) {
            if (pos == 0) {
                ratingDiet = -1;
            } else {
                ratingDiet = userDiets.get(pos - 1).getId();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private class GetUserDietsTask extends AsyncTask<Void, Void, Integer> {
        private static final String JSON_TAG_DIET_ID = "diet_id";
        private static final String JSON_TAG_DIET_NAME = "diet_name";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            //Builds the request
            Request request = new Request.Builder()
                    .url(getUserDietsUrl)
                    .build();

            try {
                Response response = client.newCall(request).execute();

                ResponseBody responseBody = response.body();
                assert responseBody != null;
                String result = responseBody.string();
                JSONArray jsonResponse = new JSONArray(result);

                spinnerArray.add("None");
                for (int dietIndex = 0; dietIndex < jsonResponse.length(); ++dietIndex) {
                    JSONObject diet = (JSONObject) jsonResponse.get(dietIndex);
                    spinnerArray.add(diet.getString(JSON_TAG_DIET_NAME));
                    userDiets.add(new DietSummary(diet.getInt(JSON_TAG_DIET_ID),
                            diet.getString(JSON_TAG_DIET_NAME)));
                }
                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            Spinner dietSpinner = findViewById(R.id.rate_restaurant_diet);
            ArrayAdapter<String> dietAdapter = new ArrayAdapter<>(RateRestaurantActivity.this,
                    android.R.layout.simple_spinner_item, spinnerArray);
            dietAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dietSpinner.setAdapter(dietAdapter);
            dietSpinner.setOnItemSelectedListener(RateRestaurantActivity.this);
            dietSpinner.setVisibility(View.VISIBLE);
        }
    }

    private class RateRestaurantTask extends AsyncTask<Void, Void, Integer> {
        private static final String REQ_RATE_RESTAURANT_ID = "restaurant";
        private static final String REQ_RATE_RESTAURANT_GRADE = "rating_grade";
        private static final String REQ_RATE_RESTAURANT_ACCESSIBILITY = "rating_accessibility";
        private static final String REQ_RATE_RESTAURANT_DIET = "diet";
        private static final String REQ_RATE_RESTAURANT_TEXT = "rating_text";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            FormBody.Builder builder = new FormBody.Builder()
                    .add(REQ_RATE_RESTAURANT_ID, "" + restaurant.getId())
                    .add(REQ_RATE_RESTAURANT_GRADE, "" + ratingGrade)
                    .add(REQ_RATE_RESTAURANT_TEXT, ratingText.getText().toString())
                    .add("user", "" + sessionManager.getUserId());

            if (ratingAccessibility != null) {
                builder.add(REQ_RATE_RESTAURANT_ACCESSIBILITY,
                        "" + RestaurantRating.Accessibility.getWorkingTypeFromId(ratingAccessibility.getId()));
            }
            if (ratingDiet != -1) {
                builder.add(REQ_RATE_RESTAURANT_DIET, "" + ratingDiet);
            }

            RequestBody formBody = builder.build();
            Request request = new Request.Builder()
                    .url(rateRestaurantUrl)
                    .post(formBody)
                    .build();

            try {
                client.newCall(request).execute();

                return 0;
            } catch (Exception e) {
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            Toast.makeText(RateRestaurantActivity.this, "Rating added!", Toast.LENGTH_SHORT).show();
            RateRestaurantActivity.this.finish();
        }
    }
}

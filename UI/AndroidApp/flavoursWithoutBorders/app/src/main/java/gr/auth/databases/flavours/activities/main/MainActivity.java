package gr.auth.databases.flavours.activities.main;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.AddRestaurantActivity;
import gr.auth.databases.flavours.activities.main.fragments.MainListFragment;
import gr.auth.databases.flavours.activities.main.fragments.MainMapFragment;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.Restaurant;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static gr.auth.databases.flavours.session.SessionManager.restaurantsUrl;

public class MainActivity extends BaseActivity {
    private static final int LOCATION_PERMISSIONS_REQUEST_CODE_FOR_DISTANCE_FILTER = 9000;
    private static final int NUM_PAGES = 2;

    private ArrayList<Restaurant> restaurants = new ArrayList<>();

    private ViewPager viewPager;
    private MenuItem menuMapItem;
    private FloatingActionButton FAB;
    private ArrayList<Integer> mSelectedRestaurantTypes = new ArrayList<>();
    private int maxDistance = -1, maxCalories = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.main_toolbar);
        toolbar.setTitle(getString(R.string.main_toolbar_title));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        createDrawer();

        FAB = findViewById(R.id.main_fab);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), AddRestaurantActivity.class);
                startActivity(intent);
            }
        });

        viewPager = findViewById(R.id.main_pager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (menuMapItem == null) {
                    return;
                }

                if (position == 0) {
                    menuMapItem.setIcon(R.drawable.ic_map_black_24dp);
                    FAB.show();
                } else {
                    menuMapItem.setIcon(R.drawable.ic_list_black_24dp);
                    FAB.hide();
                }
            }
        });

        PagerAdapter pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        MainTask mainTask = new MainTask();
        mainTask.execute();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else if (viewPager.getCurrentItem() == 1) {
            viewPager.setCurrentItem(0);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_toolbar_menu, menu);
        menuMapItem = menu.findItem(R.id.main_toolbar_menu_map);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.main_toolbar_menu_filter) {
            View menuItemView = findViewById(R.id.main_toolbar_menu_filter);
            PopupMenu popupMenu = new PopupMenu(this, menuItemView);
            popupMenu.inflate(R.menu.main_filters_popup_menu);
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getThemedContext());
                    LayoutInflater inflater = getLayoutInflater();

                    switch (item.getItemId()) {
                        case R.id.main_filters_type:
                            builder.setTitle(R.string.main_filters_menu_type)
                                    .setMultiChoiceItems(R.array.restaurant_type, null,
                                            new DialogInterface.OnMultiChoiceClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which,
                                                                    boolean isChecked) {
                                                    if (isChecked) {
                                                        // If the user checked the item, add it to the selected items
                                                        mSelectedRestaurantTypes.add(which);
                                                    } else if (mSelectedRestaurantTypes.contains(which)) {
                                                        // Else, if the item is already in the array, remove it
                                                        mSelectedRestaurantTypes.remove(Integer.valueOf(which));
                                                    }
                                                }
                                            })
                                    .setPositiveButton(R.string.main_filters_dialog_positive, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            MainTask mainTask = new MainTask();
                                            mainTask.execute();
                                        }
                                    })
                                    .setNegativeButton(R.string.main_filters_dialog_cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            mSelectedRestaurantTypes.clear();
                                        }
                                    });
                            builder.create().show();
                            break;
                        case R.id.main_filters_distance:
                            maxDistance = -1;
                            final View customDistanceDialogView = inflater.inflate(R.layout.dialog_filter_by_distance, null);
                            builder.setView(customDistanceDialogView)
                                    .setTitle(R.string.main_filters_menu_distance)
                                    .setPositiveButton(R.string.main_filters_dialog_positive, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            String distanceString = ((EditText) customDistanceDialogView
                                                    .findViewById(R.id.dialog_filter_distance)).getText().toString();
                                            if (!distanceString.isEmpty()) {
                                                maxDistance = Integer.parseInt(distanceString);
                                            }
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                                        checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                                    String[] PERMISSIONS_LOCATION = {
                                                            Manifest.permission.ACCESS_FINE_LOCATION,
                                                            Manifest.permission.ACCESS_COARSE_LOCATION};
                                                    requestPermissions(PERMISSIONS_LOCATION, LOCATION_PERMISSIONS_REQUEST_CODE_FOR_DISTANCE_FILTER);
                                                } else {
                                                    MainTask mainTask = new MainTask();
                                                    mainTask.execute();
                                                }
                                            }
                                        }
                                    })
                                    .setNegativeButton(R.string.main_filters_dialog_cancel, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            maxDistance = -1;
                                        }
                                    });
                            builder.create().show();
                            break;
                        case R.id.main_filters_calories:
                            maxCalories = -1;
                            final View customCaloriesDialogView = inflater.inflate(R.layout.dialog_filter_by_calories, null);
                            builder.setView(customCaloriesDialogView)
                                    .setTitle(R.string.main_filters_menu_calories)
                                    .setPositiveButton(R.string.main_filters_dialog_positive, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            String caloriesString = ((EditText) customCaloriesDialogView
                                                    .findViewById(R.id.dialog_filter_calories)).getText().toString();
                                            if (!caloriesString.isEmpty()) {
                                                maxCalories = Integer.parseInt(caloriesString);
                                            }
                                            MainTask mainTask = new MainTask();
                                            mainTask.execute();
                                        }
                                    })
                                    .setNegativeButton(R.string.main_filters_dialog_cancel, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            maxCalories = -1;
                                        }
                                    });
                            builder.create().show();
                            break;
                        case R.id.main_filters_clear:
                            mSelectedRestaurantTypes.clear();
                            maxDistance = -1;
                            maxCalories = -1;
                            MainTask mainTask = new MainTask();
                            mainTask.execute();
                            break;
                    }
                    return false;
                }
            });
            popupMenu.show();
            return true;
        } else if (id == R.id.main_toolbar_menu_map) {
            if (viewPager.getCurrentItem() == 0) {
                viewPager.setCurrentItem(1);
            } else if (viewPager.getCurrentItem() == 1) {
                viewPager.setCurrentItem(0);
            }
            return true;
        } else if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class MainPagerAdapter extends FragmentStatePagerAdapter {
        MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return MainListFragment.newInstance(restaurants);
            } else {
                return MainMapFragment.newInstance(restaurants);
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSIONS_REQUEST_CODE_FOR_DISTANCE_FILTER) {
            MainTask mainTask = new MainTask();
            mainTask.execute();
        }
    }

    private class MainTask extends AsyncTask<Void, Void, Integer> {
        private static final String JSON_TAG_RESTAURANT_ID = "restaurant_id";
        private static final String JSON_TAG_RESTAURANT_NAME = "restaurant_name";
        private static final String JSON_TAG_RESTAURANT_CATEGORY = "restaurant_category";
        private static final String JSON_TAG_RESTAURANT_LONGITUDE = "restaurant_longitude";
        private static final String JSON_TAG_RESTAURANT_LATITUDE = "restaurant_latitude";
        private static final String JSON_TAG_RESTAURANT_OPENING = "restaurant_opening";
        private static final String JSON_TAG_RESTAURANT_CLOSING = "restaurant_closing";
        private static final String JSON_TAG_RESTAURANT_IS_ACCEPTED = "restaurant_is_approved";

        @Override
        protected void onPreExecute() {
            restaurants.clear();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            StringBuilder requestUrl = new StringBuilder(restaurantsUrl);
            if (!mSelectedRestaurantTypes.isEmpty()) {
                requestUrl.append("?filter_restaurant_type=");
                for (int selectedTypeIndex = 0; selectedTypeIndex < mSelectedRestaurantTypes.size(); ++selectedTypeIndex) {
                    int typeIndex = mSelectedRestaurantTypes.get(selectedTypeIndex);
                    requestUrl.append(Restaurant.RestaurantType.getWorkingTypeFromId(typeIndex));
                    if (selectedTypeIndex < mSelectedRestaurantTypes.size() - 1) {
                        requestUrl.append(",");
                    }
                }
            }
            if (maxDistance != -1) {
                if (!mSelectedRestaurantTypes.isEmpty()) {
                    requestUrl.append("&");
                } else {
                    requestUrl.append("?");
                }

                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                // get the last know location from your location manager.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        String[] PERMISSIONS_LOCATION = {
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION};
                        requestPermissions(PERMISSIONS_LOCATION, LOCATION_PERMISSIONS_REQUEST_CODE_FOR_DISTANCE_FILTER);
                        return 3;
                    }
                }
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                requestUrl.append("filter_restaurant_radius=").append(maxDistance * 1. / 1000)
                        .append("&filter_restaurant_longitude=").append(location.getLongitude())
                        .append("&filter_restaurant_latitude=").append(location.getLatitude());
            }
            if (maxCalories != -1) {
                if (!mSelectedRestaurantTypes.isEmpty() || maxDistance != -1) {
                    requestUrl.append("&");
                } else {
                    requestUrl.append("?");
                }
                requestUrl.append("filter_restaurant_calories=").append(maxCalories);
            }

            mSelectedRestaurantTypes.clear();
            maxDistance = -1;
            maxCalories = -1;

            //Builds the request
            Request request = new Request.Builder()
                    .url(requestUrl.toString())
                    .build();

            try {
                Response response = client.newCall(request).execute();

                ResponseBody responseBody = response.body();
                assert responseBody != null;
                String result = responseBody.string();
                JSONArray jsonRestaurants = new JSONArray(result);

                for (int restaurantIndex = 0; restaurantIndex < jsonRestaurants.length(); ++restaurantIndex) {
                    JSONObject jsonRestaurant = jsonRestaurants.getJSONObject(restaurantIndex);
                    restaurants.add(new Restaurant(
                            jsonRestaurant.getInt(JSON_TAG_RESTAURANT_ID),
                            jsonRestaurant.getString(JSON_TAG_RESTAURANT_NAME),
                            jsonRestaurant.getString(JSON_TAG_RESTAURANT_CATEGORY),
                            jsonRestaurant.getDouble(JSON_TAG_RESTAURANT_LONGITUDE),
                            jsonRestaurant.getDouble(JSON_TAG_RESTAURANT_LATITUDE),
                            jsonRestaurant.getString(JSON_TAG_RESTAURANT_OPENING),
                            jsonRestaurant.getString(JSON_TAG_RESTAURANT_CLOSING),
                            jsonRestaurant.getBoolean(JSON_TAG_RESTAURANT_IS_ACCEPTED)));
                }
                return 1;
            } catch (Exception e) {
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            viewPager.setAdapter(null);
            PagerAdapter pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
            viewPager.setAdapter(pagerAdapter);
        }
    }
}

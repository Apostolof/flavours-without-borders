package gr.auth.databases.flavours.activities.drink.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.profile.ProfileActivity;
import gr.auth.databases.flavours.model.ItemRating;
import gr.auth.databases.flavours.utils.ItemRatingsAdapter;

import static gr.auth.databases.flavours.activities.profile.ProfileActivity.BUNDLE_USER_ID;
import static gr.auth.databases.flavours.activities.profile.ProfileActivity.BUNDLE_USER_NAME;

public class DrinkRatingsFragment extends Fragment implements
        ItemRatingsAdapter.ProfileItemRatingsAdapterInteractionListener {

    public DrinkRatingsFragment() {
        // Required empty public constructor
    }

    private static final String DRINK_RATINGS = "DRINK_RATINGS";

    private ArrayList<ItemRating> ratings;

    public static DrinkRatingsFragment newInstance(ArrayList<ItemRating> ratings) {
        DrinkRatingsFragment fragment = new DrinkRatingsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(DRINK_RATINGS, ratings);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        ratings = getArguments().getParcelableArrayList(DRINK_RATINGS);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.reusable_recycler_list, container, false);

        Context context = getContext();
        assert context != null;
        ItemRatingsAdapter itemAdapter = new ItemRatingsAdapter(context, ratings, this);
        RecyclerView mainContent = rootView.findViewById(R.id.recycler_list);
        mainContent.setAdapter(itemAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mainContent.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainContent.getContext(),
                layoutManager.getOrientation());
        mainContent.addItemDecoration(dividerItemDecoration);

        return rootView;
    }

    @Override
    public void onProfileItemRatingsAdapterInteraction(int profileID, String profileUsername) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        Bundle extras = new Bundle();
        extras.putInt(BUNDLE_USER_ID, profileID);
        extras.putString(BUNDLE_USER_NAME, profileUsername);
        intent.putExtras(extras);
        startActivity(intent);
    }
}

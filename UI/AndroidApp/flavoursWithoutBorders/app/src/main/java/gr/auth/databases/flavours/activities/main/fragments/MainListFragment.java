package gr.auth.databases.flavours.activities.main.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.main.MainAdapter;
import gr.auth.databases.flavours.base.BaseApplication;
import gr.auth.databases.flavours.model.Restaurant;
import okhttp3.Request;
import okhttp3.RequestBody;

import static gr.auth.databases.flavours.session.SessionManager.acceptRestaurantUrl;

public class MainListFragment extends Fragment implements MainAdapter.AcceptRestaurantAdapterInteractionListener {
    private static final String RESTAURANTS_TAG = "RESTAURANTS_TAG";

    private ArrayList<Restaurant> restaurants;

    public static MainListFragment newInstance(ArrayList<Restaurant> restaurants) {
        MainListFragment fragment = new MainListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(RESTAURANTS_TAG, restaurants);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        this.restaurants = getArguments().getParcelableArrayList(RESTAURANTS_TAG);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.reusable_recycler_list, container, false);

        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        MainAdapter mainAdapter = new MainAdapter(getContext(), restaurants, this);
        recyclerView.setAdapter(mainAdapter);

        return rootView;
    }

    @Override
    public void onAcceptRestaurantAdapterInteraction(Restaurant restaurant) {
        AcceptRestaurantTask acceptRestaurantTask = new AcceptRestaurantTask();
        acceptRestaurantTask.execute(restaurant.getId());
        restaurants.get(restaurants.indexOf(restaurant)).setAccepted(true);
    }

    private class AcceptRestaurantTask extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            RequestBody requestBody = RequestBody.create(null, new byte[]{});

            //Builds the request
            Request request = new Request.Builder()
                    .patch(requestBody)
                    .url(acceptRestaurantUrl + params[0] + "/")
                    .build();

            try {
                //Makes request & handles response
                BaseApplication.getInstance().getClient().newCall(request).execute();
                return 0;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
        }
    }
}

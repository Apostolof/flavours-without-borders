package gr.auth.databases.flavours.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.ItemRating;
import gr.auth.databases.flavours.model.ItemSummary;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

import static gr.auth.databases.flavours.session.SessionManager.rateDrinkUrl;
import static gr.auth.databases.flavours.session.SessionManager.rateFoodUrl;

public class RateItemActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    public static final String BUNDLE_RATE_ITEM = "BUNDLE_RATE_ITEM_TYPE";

    private ItemSummary item;
    private EditText ratingText;
    private int ratingGrade;
    private ItemRating.PortionSize ratingPortionSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_item);

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        item = extras.getParcelable(BUNDLE_RATE_ITEM);

        Toolbar toolbar = findViewById(R.id.rate_item_toolbar);
        setSupportActionBar(toolbar);
        if (item.getType() == ItemSummary.ItemType.FOOD) {
            toolbar.setTitle(getString(R.string.rate_item_toolbar_title_food));
        } else if (item.getType() == ItemSummary.ItemType.DRINK) {
            toolbar.setTitle(getString(R.string.rate_item_toolbar_title_drink));
        }

        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        Spinner gradeSpinner = findViewById(R.id.rate_item_grade);
        ArrayAdapter<CharSequence> gradeAdapter = ArrayAdapter.createFromResource(this,
                R.array.rating_grade, android.R.layout.simple_spinner_item);
        gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gradeSpinner.setAdapter(gradeAdapter);
        gradeSpinner.setOnItemSelectedListener(this);

        Spinner portionSizeSpinner = findViewById(R.id.rate_item_portion_size);
        ArrayAdapter<CharSequence> portionSizeAdapter = ArrayAdapter.createFromResource(this,
                R.array.rating_portion_size, android.R.layout.simple_spinner_item);
        portionSizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        portionSizeSpinner.setAdapter(portionSizeAdapter);
        portionSizeSpinner.setOnItemSelectedListener(this);

        ratingText = findViewById(R.id.rate_item_text);

        AppCompatButton rateButton = findViewById(R.id.rate_item_rate_btn);
        rateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateItemTask rateItemTask = new RateItemTask();
                rateItemTask.execute();
            }
        });

        createDrawer();
        drawer.setSelection(-1);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        if (parent.getId() == R.id.rate_item_grade) {
            ratingGrade = pos + 1;
        } else if (parent.getId() == R.id.rate_item_portion_size) {
            ratingPortionSize = ItemRating.PortionSize.getEnumTypeFromId(pos);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private class RateItemTask extends AsyncTask<Void, Void, Integer> {
        private static final String REQ_RATE_FOOD_ID = "food";

        private static final String REQ_RATE_DRINK_ID = "drink";

        private static final String REQ_RATE_ITEM_GRADE = "rating_grade";
        private static final String REQ_RATE_ITEM_DESCRIPTION = "rating_text";
        private static final String REQ_RATE_ITEM_PORTION_SIZE = "rating_pοrtion_size";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Request request;
            FormBody.Builder builder = new FormBody.Builder();

            if (ratingPortionSize != null) {
                builder.add(REQ_RATE_ITEM_PORTION_SIZE,
                        "" + ItemRating.PortionSize.getWorkingTypeFromId(ratingPortionSize.getId()));
            }

            if (item.getType() == ItemSummary.ItemType.FOOD) {
                builder.add(REQ_RATE_FOOD_ID, "" + item.getId())
                        .add(REQ_RATE_ITEM_GRADE, "" + ratingGrade)
                        .add(REQ_RATE_ITEM_DESCRIPTION, "" + ratingText.getText().toString())
                        .add("user", "" + sessionManager.getUserId())
                        .build();
                RequestBody formBody = builder.build();
                request = new Request.Builder()
                        .url(rateFoodUrl)
                        .post(formBody)
                        .build();
            } else {
                builder.add(REQ_RATE_DRINK_ID, "" + item.getId())
                        .add(REQ_RATE_ITEM_GRADE, "" + ratingGrade)
                        .add(REQ_RATE_ITEM_DESCRIPTION, "" + ratingText.getText().toString())
                        .add("user", "" + sessionManager.getUserId())
                        .build();
                RequestBody formBody = builder.build();
                request = new Request.Builder()
                        .url(rateDrinkUrl)
                        .post(formBody)
                        .build();
            }

            try {
                client.newCall(request).execute();

                return 0;
            } catch (Exception e) {
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            Toast.makeText(RateItemActivity.this, "Rating added!", Toast.LENGTH_SHORT).show();
            RateItemActivity.this.finish();
        }
    }
}
